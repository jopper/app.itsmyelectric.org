<?php
App::uses('AppModel', 'Model');
/**
 * TipOffType Model
 *
 * @property TipOff $TipOff
 */
class TipOffType extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'type';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TipOff' => array(
			'className' => 'TipOff',
			'foreignKey' => 'tip_off_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
