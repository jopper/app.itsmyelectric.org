<?php
App::uses('AppModel', 'Model');
/**
 * TipOffStatus Model
 *
 * @property TipOff $TipOff
 */
class TipOffStatus extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'status';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TipOff' => array(
			'className' => 'TipOff',
			'foreignKey' => 'tip_off_status_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
