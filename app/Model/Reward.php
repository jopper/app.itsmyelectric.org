<?php
App::uses('AppModel', 'Model');
/**
 * Reward Model
 *
 * @property User $User
 * @property TipOff $TipOff
 * @property ElectricCooperatives $ElectricCooperatives
 * @property PaymentSchedule $PaymentSchedule
 * @property Informer $Informer
 * @property RewardStatus $RewardStatus
 * @property MobileCashRecord $MobileCashRecord
 */
class Reward extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TipOff' => array(
			'className' => 'TipOff',
			'foreignKey' => 'tip_off_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ElectricCooperatives' => array(
			'className' => 'ElectricCooperatives',
			'foreignKey' => 'electric_cooperatives_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'PaymentSchedule' => array(
			'className' => 'PaymentSchedule',
			'foreignKey' => 'payment_schedule_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Informer' => array(
			'className' => 'Informer',
			'foreignKey' => 'informer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'RewardStatus' => array(
			'className' => 'RewardStatus',
			'foreignKey' => 'reward_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'MobileCashRecord' => array(
			'className' => 'MobileCashRecord',
			'foreignKey' => 'reward_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
