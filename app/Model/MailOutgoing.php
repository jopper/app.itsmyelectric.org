<?php
App::uses('AppModel', 'Model');
/**
 * MailOutgoing Model
 *
 * @property DeliveryStatus $DeliveryStatus
 */
class MailOutgoing extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'DeliveryStatus' => array(
			'className' => 'DeliveryStatus',
			'foreignKey' => 'delivery_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
