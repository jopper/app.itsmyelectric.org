<?php
App::uses('AppModel', 'Model');
/**
 * TipOffEvidence Model
 *
 * @property TipOff $TipOff
 */
class TipOffEvidence extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TipOff' => array(
			'className' => 'TipOff',
			'foreignKey' => 'tip_off_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
