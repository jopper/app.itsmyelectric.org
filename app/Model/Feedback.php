<?php
App::uses('AppModel', 'Model');
/**
 * Feedback Model
 *
 * @property Informer $Informer
 * @property ElectricCooperatives $ElectricCooperatives
 */
class Feedback extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Informer' => array(
			'className' => 'Informer',
			'foreignKey' => 'informer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ElectricCooperatives' => array(
			'className' => 'ElectricCooperatives',
			'foreignKey' => 'electric_cooperatives_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
