<?php
App::uses('AppModel', 'Model');
/**
 * SmsOutgoing Model
 *
 * @property Announcement $Announcement
 * @property DeliveryStatus $DeliveryStatus
 */
class SmsOutgoing extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Announcement' => array(
			'className' => 'Announcement',
			'foreignKey' => 'announcement_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'DeliveryStatus' => array(
			'className' => 'DeliveryStatus',
			'foreignKey' => 'delivery_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
