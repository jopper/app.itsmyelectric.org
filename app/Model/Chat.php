<?php
App::uses('AppModel', 'Model');
/**
 * Chat Model
 *
 * @property User $User
 * @property Informer $Informer
 * @property ElectricCooperatives $ElectricCooperatives
 * @property DeliveryStatus $DeliveryStatus
 */
class Chat extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Informer' => array(
			'className' => 'Informer',
			'foreignKey' => 'informer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ElectricCooperatives' => array(
			'className' => 'ElectricCooperatives',
			'foreignKey' => 'electric_cooperatives_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'DeliveryStatus' => array(
			'className' => 'DeliveryStatus',
			'foreignKey' => 'delivery_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
