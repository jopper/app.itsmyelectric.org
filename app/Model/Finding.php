<?php
App::uses('AppModel', 'Model');
/**
 * Finding Model
 *
 * @property TipOff $TipOff
 */
class Finding extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'findings';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TipOff' => array(
			'className' => 'TipOff',
			'foreignKey' => 'finding_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
