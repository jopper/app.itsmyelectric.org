<?php
App::uses('AppModel', 'Model');
/**
 * TipOff Model
 *
 * @property Informer $Informer
 * @property ElectricCooperatives $ElectricCooperatives
 * @property TipOffStatus $TipOffStatus
 * @property Finding $Finding
 * @property Area $Area
 * @property TipOffType $TipOffType
 * @property InitialComputation $InitialComputation
 * @property InspectionReport $InspectionReport
 * @property LoadSurvey $LoadSurvey
 * @property MobileCashRecord $MobileCashRecord
 * @property NegotiatedComputation $NegotiatedComputation
 * @property PaymentSchedule $PaymentSchedule
 * @property Reward $Reward
 * @property TipOffComment $TipOffComment
 */
class TipOff extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'code';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Informer' => array(
			'className' => 'Informer',
			'foreignKey' => 'informer_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'ElectricCooperative' => array(
			'className' => 'ElectricCooperative',
			'foreignKey' => 'electric_cooperatives_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TipOffStatus' => array(
			'className' => 'TipOffStatus',
			'foreignKey' => 'tip_off_status_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Finding' => array(
			'className' => 'Finding',
			'foreignKey' => 'finding_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Area' => array(
			'className' => 'Area',
			'foreignKey' => 'area_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'TipOffType' => array(
			'className' => 'TipOffType',
			'foreignKey' => 'tip_off_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'TipOffEvidence' => array(
			'className' => 'TipOffEvidence',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InitialComputation' => array(
			'className' => 'InitialComputation',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'InspectionReport' => array(
			'className' => 'InspectionReport',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'LoadSurvey' => array(
			'className' => 'LoadSurvey',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'MobileCashRecord' => array(
			'className' => 'MobileCashRecord',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'NegotiatedComputation' => array(
			'className' => 'NegotiatedComputation',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'PaymentSchedule' => array(
			'className' => 'PaymentSchedule',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Reward' => array(
			'className' => 'Reward',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'TipOffComment' => array(
			'className' => 'TipOffComment',
			'foreignKey' => 'tip_off_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);


	public function isOwnedBy($tipOff, $user) {
    	return $this->field('id', array('id' => $tipOff, 'informer_id' => $user)) === $tipOff;
	}



}
