<?php
App::uses('AppController', 'Controller');
/**
 * TipOffEvidences Controller
 *
 * @property TipOffEvidence $TipOffEvidence
 * @property PaginatorComponent $Paginator
 */
class TipOffEvidencesController extends AppController {


/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Security');
	

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('android_add_evidence', 'android_evidence_list', 'android_view_file'));
		$this->Security->unlockedActions = array('android_add_evidence', 'android_evidence_list', 'android_view_file');
	}



/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TipOffEvidence->recursive = 0;
		$this->loadModel('TipOff');
		$tipOffs = $this->TipOff->find('list', array('conditions' => array('TipOff.informer_id' => AuthComponent::User('id')),
													'fields' => array('TipOff.id')));
		$this->paginate = array('conditions' => array('TipOffEvidence.tip_off_id' => $tipOffs));
		$this->set('tipOffEvidences', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TipOffEvidence->exists($id)) {
			throw new NotFoundException(__('Invalid tip off evidence'));
		}
		$options = array('conditions' => array('TipOffEvidence.' . $this->TipOffEvidence->primaryKey => $id));
		$this->set('tipOffEvidence', $this->TipOffEvidence->find('first', $options));
	}

	public function view_file($id = null, $toId = null) {
		$this->loadModel('TipOff');
		if (!$this->TipOff->exists($toId)) {
			$this->Session->setFlash(__('The report evidence file could not be found..'), 'flash/danger');
			$this->redirect(array('controller' => 'tip_off_evidences', 'action' => 'index'));
			//throw new NotFoundException(__('Invalid tip off evidence'));
		}
		if (!$this->TipOffEvidence->exists($id)) {
			$this->Session->setFlash(__('The report evidence file could not be found..'), 'flash/danger');
			$this->redirect(array('controller' => 'tip_off_evidences', 'action' => 'index'));
			//throw new NotFoundException(__('Invalid tip off evidence'));
		}
		$options = array('conditions' => array('AND' => array( 
																array('TipOffEvidence.' . $this->TipOffEvidence->primaryKey => $id),
																array('TipOffEvidence.tip_off_id' => $toId)
															)));
		if($tipOffEvidence = $this->TipOffEvidence->find('first', $options)){
			$this->response->file(EVIDENCE_DIR. $tipOffEvidence['TipOffEvidence']['new_file_name'], array('download' => false, 'name' => $tipOffEvidence['TipOffEvidence']['file_name']));
		} else {
			$this->Session->setFlash(__('The report evidence file could not be found or you are not authorized.'), 'flash/danger');
			$this->redirect(array('controller' => 'tip_off_evidences', 'action' => 'index'));
		}
		return $this->response;
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			//$this->TipOffEvidence->create();
			ini_set('upload_max_filesize', '64M');
			set_time_limit(600);
			$filename = null;
			$newFileName = null;
			if (!empty($this->request->data['TipOffEvidence']['file_name']['tmp_name']) && is_uploaded_file($this->request->data['TipOffEvidence']['file_name']['tmp_name'])) {
				    // Strip path information
				    $image_dimension = getimagesize($this->request->data['TipOffEvidence']['file_name']['tmp_name']);
				    $size = $this->request->data['TipOffEvidence']['file_name']['size'];
					$filename = Inflector::slug(pathinfo($this->request->data['TipOffEvidence']['file_name']['name'], PATHINFO_FILENAME)).'.'.pathinfo($this->request->data['TipOffEvidence']['file_name']['name'], PATHINFO_EXTENSION);

				    //$filename = basename($this->request->data['TipOffEvidence']['file']['name']);
					$newFileName = date('Y_m_d_H_i_s').'_'.$this->request->data['TipOffEvidence']['tip_off_code'].'_'.$filename; 
				    move_uploaded_file($this->request->data['TipOffEvidence']['file_name']['tmp_name'], EVIDENCE_DIR. $newFileName);
				
				
				$image_width = $image_dimension[0];
				$image_height = $image_dimension[1];

				$this->request->data['TipOffEvidence']['user_id'] = AuthComponent::User('id');
				// Set the file-name only to save in the database
				$this->request->data['TipOffEvidence']['file_name'] = $filename;
				$this->request->data['TipOffEvidence']['new_file_name'] = $newFileName;
				$this->request->data['TipOffEvidence']['size'] = $size;
				$this->request->data['TipOffEvidence']['dimension'] = $image_width.' x '.$image_height;


				if ($this->TipOffEvidence->save($this->request->data)) {
					$this->Session->setFlash(__('The report evidence file has been saved'), 'flash/success');
					$this->redirect(array('controller' => 'tip_offs', 'action' => 'view', $this->request->data['TipOffEvidence']['tip_off_id']));
					/*$this->redirect(array('controller' => 'report_evidence_files', 'action' => 'index'));*/
					}
			} else {
				$this->Session->setFlash(__('The report evidence file could not be saved. Please, try again.'), 'flash/danger');
				$this->redirect(array('controller' => 'tip_offs', 'action' => 'view', $this->request->data['TipOffEvidence']['tip_off_id']));
			}
		}
		$tipOffs = $this->TipOffEvidence->TipOff->find('list');
		$this->set(compact('tipOffs'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->TipOffEvidence->id = $id;
		if (!$this->TipOffEvidence->exists($id)) {
			throw new NotFoundException(__('Invalid tip off evidence'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TipOffEvidence->save($this->request->data)) {
				$this->Session->setFlash(__('The tip off evidence has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The tip off evidence could not be saved. Please, try again.'), 'flash/danger');
			}
		} else {
			$options = array('conditions' => array('TipOffEvidence.' . $this->TipOffEvidence->primaryKey => $id));
			$this->request->data = $this->TipOffEvidence->find('first', $options);
		}
		$tipOffs = $this->TipOffEvidence->TipOff->find('list');
		$this->set(compact('tipOffs'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TipOffEvidence->id = $id;
		if (!$this->TipOffEvidence->exists()) {
			throw new NotFoundException(__('Invalid tip off evidence'));
		}
		if ($this->TipOffEvidence->delete()) {
			$this->Session->setFlash(__('Tip off evidence deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tip off evidence was not deleted'), 'flash/danger');
		$this->redirect(array('action' => 'index'));
	}





	public function android_add_evidence() {
			$this->autoRender = false;
		if ($this->request->is('post')) {

			//if(!empty($_POST['tag']) && $_POST['tag'] == 'itsmyelectric_android_evidence'){

				//print_r($_POST['posted_data']);

				ini_set('upload_max_filesize', '64M');
				set_time_limit(600);
				$filename = null;
				$newFileName = null;

				$this->loadModel('TipOff');
				
				$TipOff = $this->TipOff->find('first', array('conditions' => array('TipOff.id' => $_GET['report_id']), 'fields' => array('TipOff.code')));



						//  $my_file = EVIDENCE_DIR.'file.txt';
						//  $handle = fopen($my_file, 'w') or die('Cannot open file:  '.$my_file);
						// foreach ($_GET as $key => $value){
						//  $data = "Field ".htmlspecialchars($key)." is ".htmlspecialchars($value)."<br>";
						// fwrite($handle, $data);
						// }

				if (!empty($_FILES['uploaded_file']['tmp_name']) && is_uploaded_file($_FILES['uploaded_file']['tmp_name'])) {
					    // Strip path information
					    $image_dimension = getimagesize($_FILES['uploaded_file']['tmp_name']);
					    $size = $_FILES['uploaded_file']['size'];
						$filename = Inflector::slug(pathinfo($_FILES['uploaded_file']['name'], PATHINFO_FILENAME)).'.'.pathinfo($_FILES['uploaded_file']['name'], PATHINFO_EXTENSION);

						$newFileName = date('Y_m_d_H_i_s').'_'.$TipOff['TipOff']['code'].'_'.$filename; 
					    move_uploaded_file($_FILES['uploaded_file']['tmp_name'], EVIDENCE_DIR. $newFileName);
						

					$image_width = $image_dimension[0];
					$image_height = $image_dimension[1];

					$this->request->data['TipOffEvidence']['user_id'] = $_GET['informer_id'];
					// Set the file-name only to save in the database
					$this->request->data['TipOffEvidence']['file_name'] = $filename;
					$this->request->data['TipOffEvidence']['new_file_name'] = $newFileName;
					$this->request->data['TipOffEvidence']['size'] = $size;
					$this->request->data['TipOffEvidence']['dimension'] = $image_width.' x '.$image_height;
					$this->request->data['TipOffEvidence']['details'] = $_GET['details'];
					$this->request->data['TipOffEvidence']['tip_off_id'] = $_GET['report_id'];

					if ($this->TipOffEvidence->save($this->request->data)) {
						//$this->Session->setFlash(__('The report evidence file has been saved'), 'flash/success');
						//$this->redirect(array('controller' => 'tip_offs', 'action' => 'view', $this->request->data['TipOffEvidence']['tip_off_id']));
						
						$data = array('success' => 1, 'Results' => $responses);

					} else {
						$data = array('success' => 'not save');
					}

				} else {
					$data = array('success' => 'not uploaded');
				}
			// } else {
			// 	$data = array('success' => 'empty tag');
			// }	

			echo json_encode($data);
		}
	}




	public function android_evidence_list(){
		
		$this->autoRender = false;
		//$this->render('itsmyelectric_android_evidence');

		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_evidence') {

				$responses = array();

				if(isset($_POST['report_id'])){
					$tipOffId = $_POST['report_id'];
					$responses = $this->TipOffEvidence->findAllByTipOffId($tipOffId);
				}	
				if(isset($_POST['informer_id'])){
					$informerId = $_POST['informer_id'];
					$this->loadModel('TipOff');
					$tipOffs = $this->TipOff->findAllByInformerId($informerId);
					$tipOffIds = Set::classicExtract($tipOffs, '{n}.TipOff.id');
					$responses = $this->TipOffEvidence->find('all', array('conditions' => array('TipOff.id' => $tipOffIds)));
				}	

					$this->TipOffEvidence->recursive = 1;
					if($responses){
						//$data["success"] = 1;
						$data = array('success' => 1, 'Results' => $responses);
						
					}	else {
						$data = array('success' => null);
					}	
						
					echo json_encode($data);

			}
			
		}
		
	}

	public function android_view_file($id = null) {

		$this->autoRender = false;
		if (!$this->TipOffEvidence->exists($id)) {
			throw new NotFoundException(__('Invalid tip off evidence'));
		}
		$options = array('conditions' => array('TipOffEvidence.' . $this->TipOffEvidence->primaryKey => $id));
		$tipOffEvidence = $this->TipOffEvidence->find('first', $options);
		$this->response->file(EVIDENCE_DIR. $tipOffEvidence['TipOffEvidence']['new_file_name'], array('download' => false, 'name' => $tipOffEvidence['TipOffEvidence']['file_name']));
		return $this->response;
	}




}
