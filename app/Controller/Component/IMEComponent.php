<?php


App::uses('Component', 'Controller');

class IMEComponent extends Component {

	public function generateCode($s_length = null, $n_length = null) {

			$str='';
			$num='';
			$total_length = $s_length + $n_length ;
			$continue = true;
			do {

				if ($s_length) {
					$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";	
					$str = '';
					$size = strlen( $chars );
					for( $i = 0; $i < $s_length; $i++ ) {
						$str .= $chars[ rand( 0, $size - 1 ) ];
					}
					$str = strtoupper($str);
				} else {
					$str = '';
				}	
	
				if ($n_length) {
					$nums = "123456789";
					$num = '';
					$size = strlen($nums);
					for( $i = 0; $i < $n_length; $i++ ) {
						$num .= $nums[ rand( 0, $size - 1 ) ];
					}
					$num = strtoupper($num);
				} else {
					$num = '';
				}
		
					$gen_string = strlen($str) + strlen($num);
					
					if ($total_length == $gen_string) {
						$continue = false;
					}
			
			} while  ($continue);
			
			return $str.$num;

		}



		public function createInformer($mobile = null, $email = null, $type = null, $username = null,  $password = null, $electricCoopId = null){

			$this->Informer = ClassRegistry::init('Informer');
			if ($this->Informer->findByMobile($mobile)) {
						return false;
				
				}else if ($this->Informer->findByEmail($email)) {
				
						return false;	

				} else {
						
						$continue = true;
						do {
							$Informer['Informer']['code']  = trim($this->generateCode(null, 10));

							if($username){
								$Informer['Informer']['username'] = $username;
							} else {
								$Informer['Informer']['username'] = $Informer['Informer']['code'];
							}	

							if($mobile){
								$Informer['Informer']['mobile'] = trim($mobile);
							}
							if($email){
								$Informer['Informer']['email'] = trim($email);
							}
							
							if($type){
								$Informer['Informer']['type'] = trim($type);
							}

							if($password){
								$Informer['Informer']['password'] = AuthComponent::password(trim($password));
								$informerPass = $password;
							} else {	
								$informerPass = $this->generateCode(4, 4);
								$Informer['Informer']['password'] = AuthComponent::password(trim($informerPass));
							}

							if($electricCoopId){
								$Informer['Informer']['electric_cooperatives_id'] = trim($electricCoopId);
							}	

							if(!$this->Informer->findByCode($Informer['Informer']['code'])){
								if ($this->Informer->save($Informer)) {
									$continue = false;
								}
							}

						} while ($continue) ;
										
						// sleep(2);
						$informerData = array('codename' => $Informer['Informer']['code'], 'password' => $informerPass);
						return $informerData;

				}

		}

		public function notifyUser($reporter_id = null, $message = null){
       			$this->User = ClassRegistry::init('User');
       			$userData = $this->User->findById($reporter_id);  
				$success = 0;	
		
					
					if(!empty($userData['User']['mobile'])){
						$this->SmsOutgoing = ClassRegistry::init('SmsOutgoing');
						$this->SmsOutgoing->create();
						$sms_out['SmsOutgoing']['message'] = $message;
						$sms_out['SmsOutgoing']['recipient'] = $userData['User']['mobile'];
						$sms_out['SmsOutgoing']['delivery_status_id'] = 1;
						$sms_out['SmsOutgoing']['date_created'] = date("Y-m-d H:i:s");
				
						if ($this->SmsOutgoing->save($sms_out)){
							$success = 1;
						} 
					} 
							
								
					if(!empty($userData['User']['email'])){
						$this->MailOutgoing = ClassRegistry::init('MailOutgoing');
						$this->MailOutgoing->create();
						$mail_out['MailOutgoing']['recipient'] = $userData['User']['email'];
						$mail_out['MailOutgoing']['from'] = 'notification@itsmyelectric.org';
						$mail_out['MailOutgoing']['subject'] = 'ITSMYELECTRIC.ORG Notification';
						$mail_out['MailOutgoing']['message'] = $message;
						$mail_out['MailOutgoing']['delivery_status_id'] = 1;
						$mail_out['MailOutgoing']['date_created'] = date("Y-m-d H:i:s");
				
						if ($this->MailOutgoing->save($mail_out)){
							$success = 1;
						} 
					}
					
				
				return $success;
			
       		}


 //    public function update_count($ecId = null, $informerId = null, $type = null $count = null){
 //    	$this->Informer = ClassRegistry::init('User');
 //    	$this->ElectricCooperative = ClassRegistry::init('ElectricCooperative');
 //    	$this->Feedback = ClassRegistry::init('Feedback');

 //    	$informerData = $this->Informer->findById($informerId);

 //    	if($count == "feedbacks"){
	// 		if(!empty($type)){
	// 			if($type = '[ + ]'){

	// 				$positiveFeedbackCounts = explode(",", $informerData['Informer']['positive_feedback_count']);

	// 				foreach ($positiveFeedbackCounts  as $key => $value) {
	// 						$pData = explode("/", $value);
	// 						 if($ecId == $pData[0]){
	// 						 	array_replace($positiveFeedbackCounts, array($key => $pData[0]."/".$pData[1]+1))
	// 						 }
	// 				}
	// 				$this->Informer->create();
	// 				$informerUpdate['Informer']['positive_feedback_count'] = implode(",", $positiveFeedbackCounts);
	// 				$this->Informer->save($informerUpdate);


	// 			} else {

	// 				$negativeFeedbackCounts = explode(",", $informerData['Informer']['negative_feedback_count']);

	// 				foreach ($negativeFeedbackCounts  as $key => $value) {
	// 					$pData = explode("/", $value);
	// 						 if($ecId == $nData[0]){
	// 						 	array_replace($negativeFeedbackCounts, array($key => $nData[0]."/".$nData[1]+1))
	// 						 }
	// 				}
	// 				$this->Informer->create();
	// 				$informerUpdate['Informer']['negative_feedback_count'] = implode(",", $negativeFeedbackCounts);
	// 				$this->Informer->save($informerUpdate);		 
	// 			}
	// 		}
	// 	}
		
	// 	if($count == 'tip_offs'){

	// 	}	

	// }   			


}
?>		