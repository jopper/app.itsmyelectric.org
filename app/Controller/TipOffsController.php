<?php
App::uses('AppController', 'Controller');
/**
 * TipOffs Controller
 *
 * @property TipOff $TipOff
 * @property PaginatorComponent $Paginator
 */
class TipOffsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Security');


	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('add_new_report', 'android_add_new_report', 'android_report_list', 'android_add_feedback', 'android_ec_list', 'checkSmsIncoming', 'checkSmsOutgoing', 'smsSender', 'nexmo', 'sms'));
		$this->Security->unlockedActions = array('add_new_report', 'android_add_new_report', 'android_report_list', 'android_add_feedback', 'android_ec_list' , 'checkSmsIncoming', 'checkSmsOutgoing', 'smsSender', 'nexmo', 'sms');
	}

	public function isAuthorized($user) {
	    // All registered users can add posts
	    if ($this->action === 'add' || $this->action === 'index') {
	        return true;
	    }

	    // The owner of a post can edit and delete it
	    if (in_array($this->action, array('view', 'edit', 'delete'))) {
	        $postId = $this->request->params['pass'][0];
	        if ($this->TipOff->isOwnedBy($postId, $user['id'])) {
	            return true;
	        }
	    }

	    //return parent::isAuthorized($user);
	}
	// test 
	public function sms($sampText) 
	{	
		$this->layout = 'ajax';
		$this->autoRender = false;
		$mobileNum = "639159941127"; // replaced $_POST['mobile_number']

	//	try {
			
			//if (isset($_POST['message_type'])) {
				// REPORT / [ELECTRIC COMPANY] / [SUSPECTED ADDRESS OR LOCATION] / [DESCRIPTION OF THEFT]
				$error = false;
				$errMessage = "";
				//$message_type = $_POST['message_type'];

				//if ($message_type == 'incoming') {
					$this->loadModel('SmsIncoming');
					$this->loadModel('RequestLog');
					$this->loadModel('Informer');
					$this->loadModel('ElectricCooperative');
					//$this->loadModel('Tipoff');
					$this->IME = $this->Components->load('IME');

					
					$this->RequestLog->set([
						'message' => 'message type is incoming'
					]);
					error_log("message");
					$this->RequestLog->save();
					$sms = $this->SmsIncoming->find('all');

					$message = explode('-', trim($sampText));
					//$message = explode('/', trim($_POST["message"]));
					if (isset($message[0])) {
						$keyword = strtoupper(trim($message[0]));

					} else {
						$error = true;
						$errMessage .= " keyword is not valid or not defined ";
					}

					if (isset($message[1])) {
						$requested_EC_code = strtoupper(trim($message[1]));
						$EC = $this->ElectricCooperative->findByCode($requested_EC_code); 
						if ($EC) {
							$EC_code = $EC['ElectricCooperative']['code'];
							$EC_id = $EC['ElectricCooperative']['id'];
							// generate new code and check if it's available.
							$continue = true;
							do {
								$tipOffCode = $EC_code.$this->IME->generateCode(3, 5);
								if(!$this->ElectricCooperative->findByCode($tipOffCode)) {
									$continue = false;
								}
							} while ($continue);
							
						} else {
							$EC_id = 1;
							$error = true;
							$errMessage = " Electric Company Code is not valid or not defined ";
						}
					} else {
						$error = true;
						$errMessage = " Electric Company is not valid or not defined ";
					}

					if (isset($message[2])) {
						$suspectedAddress = trim($message[2]);

					} else {
						$error = true;
						$errMessage = " Address is not defined ";
					}

					if (isset($message[3])) {
						$description = trim($message[3]);

					} else {
						$error = true;
						$errMessage = " Description is not defined ";
					} 
					

					if ($error != true) {
						// check informer if it's not registered yet. if so, create new informer data
						if ($this->Informer->findByMobile($mobileNum)) {
							$informer = $this->Informer->findByMobile($mobileNum);
							$this->TipOff->create();
								$this->request->data['TipOff']['code'] = trim($tipOffCode);
								$this->request->data['TipOff']['informer_id'] = $informer['Informer']['id'];
								$this->request->data['TipOff']['electric_cooperatives_id'] = $EC_id; // additional column
								$this->request->data['TipOff']['address'] = $suspectedAddress; // additinal column
								$this->request->data['TipOff']['description'] = $description; // additional column
								$this->request->data['TipOff']['area_id'] = 0;
								$this->request->data['TipOff']['tip_off_status_id'] = 1;
								$this->request->data['TipOff']['finding_id'] = 1; // None
								$this->request->data['TipOff']['created'] = date('Y-m-d H:i:s');
								$this->request->data['TipOff']['tip_off_type_id'] = 2;
								
								if ($this->TipOff->save($this->request->data)) {
									$tipOffID = $this->TipOff->getLastInsertID();
									// $notifyMessage = "Thank you for your report. Your report code is ".$tipOffCode." your codename is ".$this->request->data['Informer']['username']. " with password of"; 
									// 	echo "Success";
									//$this->IME->smsSender($_POST['mobile_number'], $notifyMessage);
								} else {
									$this->RequestLog->set([
										'message' => 'saving tipoff failed'
									]);
									$this->RequestLog->save();
									echo "Saving Tipoff Failed";
									//$this->IME->smsSender("639159941127", "saving tipoff failed");
								}
								$this->SmsIncoming->create();
								$this->request->data['SmsIncoming']['sender'] = $mobileNum;
								$this->request->data['SmsIncoming']['message'] = "hayahayahay";
								$this->request->data['SmsIncoming']['date_received'] = date('Y-m-d H:i:s');;
								$this->request->data['SmsIncoming']['source'] = 'Chikka';

								if ($this->SmsIncoming->save($this->request->data)) {
									echo "Accepted";
								} else {
									echo "Error";
								}
						} else {
							$this->Informer->create();
							$this->request->data['Informer']['code'] = trim($this->IME->generateCode(null, 10));
							$this->request->data['Informer']['username'] = $this->request->data['Informer']['code'];
							$this->request->data['Informer']['mobile'] = $mobileNum;
							$this->request->data['Informer']['type'] = 'Reporter';
							$informerPass = $this->IME->generateCode(4, 4);
							$this->request->data['Informer']['password'] = $informerPass;
							$this->request->data['Informer']['electric_cooperatives_id'] = trim($EC_id);
							
							if ($this->Informer->save($this->request->data)) { 
								$informer = $this->Informer->findByMobile($mobileNum);
								$this->TipOff->create();
								$this->request->data['TipOff']['code'] = trim($tipOffCode);
								$this->request->data['TipOff']['informer_id'] = $informer['Informer']['id'];
								$this->request->data['TipOff']['area_id'] = 0;
								$this->request->data['TipOff']['tip_off_status_id'] = 1;
								$this->request->data['TipOff']['finding_id'] = 1; // None
								$this->request->data['TipOff']['created'] = date('Y-m-d H:i:s');
								$this->request->data['TipOff']['tip_off_type_id'] = 2;
								
								if ($this->TipOff->save($this->request->data)) {
									$tipOffID = $this->TipOff->getLastInsertID();
									$notifyMessage = "Thank you for your report. Your report code is ".$tipOffCode." your codename is ".$this->request->data['Informer']['username']. " with password of ". $informerPass; 
										echo "Success";
									//$this->IME->smsSender($_POST['mobile_number'], $notifyMessage);
								} else {
									$this->RequestLog->set([
										'message' => 'saving tipoff failed'
									]);
									$this->RequestLog->save();
									echo "Saving Tipoff Failed";
									//$this->IME->smsSender("639159941127", "saving tipoff failed");
								}
								$this->SmsIncoming->create();
								$this->request->data['SmsIncoming']['sender'] = $mobileNum;
								$this->request->data['SmsIncoming']['message'] = 'hayahay';
								$this->request->data['SmsIncoming']['date_received'] = date('Y-m-d H:i:s');;
								$this->request->data['SmsIncoming']['source'] = 'Chikka';

								if ($this->SmsIncoming->save($this->request->data)) {
									echo "Accepted";
								} else {
									echo "Error";
								}
							} else {
								$this->RequestLog->set([
									'message' => 'saving informer failed'
								]);
								$this->RequestLog->save();
								echo "Saving Informer Failed";
								//$this->IME->smsSender("639159941127", "saving informer failed");
							}
						}
					
					} else {
						echo $errMessage;
						//$this->IME->smsSender("639159941127", $errMessage);
					}

				//}

			//} else {
			//		$this->loadModel('RequestLog');
			//		$this->RequestLog->set([
			//					'message' => 'message type is incoming first'
			//				]);
			//	echo "Error";
			//}
		// } catch (Exception $e) {
		// 	echo "Error";
		// }
		
	}
	
	// CHECK SMS INCOMING TEST 
	public function checkSmsIncoming() 
	{	
		$this->layout = 'ajax';
		$this->autoRender = false;

		/* IF $_POST['message_type'] is set */
		if (isset($_POST['message_type'])) {
			$error = false;
			$errMessage = "";
			$message_type = $_POST['message_type'];
			/* IF $message_type == 'incoming' */
			if ($message_type == "incoming"){
				// load other models
				$this->loadModel('SmsIncoming');
				$this->loadModel('RequestLog');
				$this->loadModel('Informer');
				$this->loadModel('ElectricCooperative');
				$mobileNum = $_POST['mobile_number'];
				$testString = "";
				// load component /Component/IMEComponent.php 
				$this->IME = $this->Components->load('IME');
                // auto-generated 32 chars code used for message_id parameter 
				$message_id =  $this->IME->generateCode(null, 32);

				// logs
				$this->RequestLog->set([
					'message' => 'new report'
				]);
				$this->RequestLog->save();

				// explode sms syntax i.e.: REPORT/BATELEC2/13 Elm Street, Manila/Wire-tapping(Jumper) 
				$message = explode('/', trim($_POST['message']));
				if (isset($message[0])) {
					$keyword = strtoupper(trim($message[0]));
					$testString .= "-".$keyword."-";
				} else {
					$error = true;
					$errMessage .= " keyword is not valid or not defined ";
				}

				if (isset($message[1])) {
					$requested_EC_code = strtoupper(trim($message[1]));
					$testString .= "".$requested_EC_code."-";
					$EC = $this->ElectricCooperative->findByCode($requested_EC_code); 
					if ($EC) {
						$EC_code = $EC['ElectricCooperative']['code'];
						$EC_id = $EC['ElectricCooperative']['id'];
						// generate new code and check if it's available.
						$continue = true;
						do {
							$tipOffCode = $EC_code.$this->IME->generateCode(3, 5);
							if(!$this->ElectricCooperative->findByCode($tipOffCode)) {
								$continue = false;
							}
						} while ($continue);
						
					} else {
						$error = true;
						$errMessage = " Electric Company Code is not valid or not defined ";
					}
				} else {
					$error = true;
					$errMessage = " Electric Company is not valid or not defined ";
				}

				if (isset($message[2])) {
					$suspectedAddress = trim($message[2]);
					$testString .= "-".$suspectedAddress."-";
				} else {
					$error = true;
					$errMessage = " Address is not defined ";
				}

				if (isset($message[3])) {
					$description = trim($message[3]);
					$testString .= "-".$description."-";
				} else {
					$error = true;
					$errMessage = " Description is not defined ";
				} 

				/* IF $error is not true */
				if ($error != true){
					/* CHECK IF INFORMER IS NOT REGISTERED YET. IF SO, CREATE NEW INFORMER DATA */
					if ($this->Informer->findByMobile($mobileNum)) {
							$informer = $this->Informer->findByMobile($mobileNum);
							$this->TipOff->create();
							$this->request->data['TipOff']['code'] = trim($tipOffCode);
							$this->request->data['TipOff']['informer_id'] = $informer['Informer']['id'];
							$this->request->data['TipOff']['electric_cooperatives_id'] = $EC_id; 
							$this->request->data['TipOff']['address'] = $suspectedAddress; 
							$this->request->data['TipOff']['description'] = $description; 
							$this->request->data['TipOff']['area_id'] = 0;
							$this->request->data['TipOff']['tip_off_status_id'] = 1;
							$this->request->data['TipOff']['finding_id'] = 1; // None
							$this->request->data['TipOff']['created'] = date('Y-m-d H:i:s');
							$this->request->data['TipOff']['tip_off_type_id'] = 2;
								
							if ($this->TipOff->save($this->request->data)) {
								$tipOffID = $this->TipOff->getLastInsertID();
								$notifyMessage = "Thank you for your report. Your report code is ".$tipOffCode.". Your codename is ".$informer['Informer']['username']; 
									//echo "Success";
								//$this->IME->smsSender($_POST['mobile_number'], $notifyMessage);
							} else {
								$this->RequestLog->set([
									'message' => 'saving tipoff failed'
								]);
								$this->RequestLog->save();
								$notifyMessage = "Whoops, something went wrong, please try again";
							}
							$this->SmsIncoming->create();
							$this->request->data['SmsIncoming']['sender'] = $mobileNum;
							$this->request->data['SmsIncoming']['message'] = "new report from existing user";
							$this->request->data['SmsIncoming']['date_received'] = date('Y-m-d H:i:s');;
							$this->request->data['SmsIncoming']['source'] = 'Chikka';

							if ($this->SmsIncoming->save($this->request->data)) {
								echo "Accepted";  
								$arr_post_body = array(
                                    "message_type" => "REPLY",
                                    "mobile_number" => $mobileNum,
                                    "shortcode" => "29290999999",
                                    "message_id" => $message_id,
                                    "request_id" => $_POST['request_id'],
                                    "request_cost" => "FREE",
                                    "message" => urlencode($notifyMessage),
                                    "client_id" => "a4a4cd6894d813c5d8201bb549f9d04ad67bede385b5be2b327570643e685e7c",
                                    "secret_key" => "dd929e43d159b5ce524de110b160b360eb0cbd6b7e24c8f14a7d1d8df94a766d"
                                );     

                            	$query_string = "";
                                foreach($arr_post_body as $key => $frow)
                                {
                                    $query_string .= '&'.$key.'='.$frow;
                                }

                                $URL = "https://post.chikka.com/smsapi/request";

                                $curl_handler = curl_init();
                                curl_setopt($curl_handler, CURLOPT_URL, $URL);
                                curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
                                curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
                                curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
                                $response = curl_exec($curl_handler);
                                curl_close($curl_handler); 

                                exit(0);                           
							} else {
								echo "Error";
							}
					} else {
							$this->Informer->create();
							$this->request->data['Informer']['code'] = trim($this->IME->generateCode(null, 10));
							$this->request->data['Informer']['username'] = $this->request->data['Informer']['code'];
							$this->request->data['Informer']['mobile'] = $mobileNum;
							$this->request->data['Informer']['type'] = 'Reporter';
							$informerPass = $this->IME->generateCode(4, 4);
							$this->request->data['Informer']['password'] = $informerPass;
							$this->request->data['Informer']['electric_cooperatives_id'] = trim($EC_id);
							
							if ($this->Informer->save($this->request->data)) { 
								$informer = $this->Informer->findByMobile($mobileNum);
								$this->TipOff->create();
								$this->request->data['TipOff']['code'] = trim($tipOffCode);
								$this->request->data['TipOff']['informer_id'] = $informer['Informer']['id'];
	                            $this->request->data['TipOff']['electric_cooperatives_id'] = $EC_id; // additional column
								$this->request->data['TipOff']['address'] = $suspectedAddress; // additinal column
								$this->request->data['TipOff']['description'] = $description; // additional column
								$this->request->data['TipOff']['area_id'] = 0;
								$this->request->data['TipOff']['tip_off_status_id'] = 1;
								$this->request->data['TipOff']['finding_id'] = 1; // None
								$this->request->data['TipOff']['created'] = date('Y-m-d H:i:s');
								$this->request->data['TipOff']['tip_off_type_id'] = 2;
								if ($this->TipOff->save($this->request->data)) {
									$tipOffID = $this->TipOff->getLastInsertID();
									$notifyMessage = "Thank you for your report. Your report code is ".$tipOffCode.". Your codename is ".$this->request->data['Informer']['username']; 
										//echo "Success";
									//$this->IME->smsSender($_POST['mobile_number'], $notifyMessage);
								} else {
									$this->RequestLog->set([
										'message' => 'saving tipoff failed'
									]);
									$this->RequestLog->save();
									$notifyMessage = "Whoops, something went wrong, please try again";
									$this->SmsIncoming->create();
									$this->request->data['SmsIncoming']['sender'] = $mobileNum;
									$this->request->data['SmsIncoming']['message'] = 'new report from new user';
									$this->request->data['SmsIncoming']['date_received'] = date('Y-m-d H:i:s');
									$this->request->data['SmsIncoming']['source'] = 'Chikka';
									if ($this->SmsIncoming->save($this->request->data)) {
										echo "Accepted";   
										$arr_post_body = array(
	                                        "message_type" => "REPLY",
	                                        "mobile_number" => $mobileNum,
	                                        "shortcode" => "29290999999",
	                                        "message_id" => $message_id,
	                                        "request_id" => $_POST['request_id'],
	                                        "request_cost" => "FREE",
	                                        "message" => urlencode($notifyMessage),
	                                        "client_id" => "a4a4cd6894d813c5d8201bb549f9d04ad67bede385b5be2b327570643e685e7c",
	                                        "secret_key" => "dd929e43d159b5ce524de110b160b360eb0cbd6b7e24c8f14a7d1d8df94a766d"
	                                    );     

                                    	$query_string = "";
	                                    foreach($arr_post_body as $key => $frow)
	                                    {
	                                        $query_string .= '&'.$key.'='.$frow;
	                                    }

	                                    $URL = "https://post.chikka.com/smsapi/request";

	                                    $curl_handler = curl_init();
	                                    curl_setopt($curl_handler, CURLOPT_URL, $URL);
	                                    curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
	                                    curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
	                                    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
	                                    $response = curl_exec($curl_handler);
	                                    curl_close($curl_handler); 

	                                    exit(0);                            
									} else {
										echo "Error";
									}
								}
							}
					}
					/* END CHECK IF inFORMER */
				}
				/*- END IF $error is not true  */
			}	
			/*-- END IF $message_type == 'incoming'*/
		}
		/*-- END IF $_POST['message_type'] is set */
		
	}
	// Sms Sender 
	public function smsSender() {
		$this->layout = 'ajax';
		$this->autoRender = false;
		$this->IME = $this->Components->load('IME');
		$message_id =  $this->IME->generateCode(null, 32);
			$arr_post_body = array(
		        "message_type" => "SEND",
		        "mobile_number" => "639208714205",
		        "shortcode" => "29290999999",
		        "message_id" => $message_id,
		        "message" => urlencode('test'),
		        "client_id" => "a4a4cd6894d813c5d8201bb549f9d04ad67bede385b5be2b327570643e685e7c",
		        "secret_key" => "dd929e43d159b5ce524de110b160b360eb0cbd6b7e24c8f14a7d1d8df94a766d"
		    );

		    $query_string = "";
		    foreach($arr_post_body as $key => $frow) 
		    {
		        $query_string .= '&'.$key.'='.$frow;
		    }

		    $URL = "https://post.chikka.com/smsapi/request";
		    //error_log("sms sender!!!");
		    $curl_handler = curl_init();
		    curl_setopt($curl_handler, CURLOPT_URL, $URL);
		    curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
		    curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
		    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
		    $response = curl_exec($curl_handler);
		    curl_close($curl_handler);
		    
		    exit(0);
	}

	// CHECK SMS OUTGOING TEST 
	public function checkSmsOutgoing()
	{
		$this->layout = 'ajax';
		$this->autoRender = false;


	  try
	    {
	        $message_type = $_POST["message_type"];
	    }
	    catch (Exception $e)
	    {
	        echo "Error";
	        exit(0);
	    }

	    if (strtoupper($message_type) == "outgoing")
	    {
	        try
	        {
	            // Retrieve the parameters from the body
	            $message_id = $_POST["message_id"];
	            $mobile_number = $_POST["mobile_number"];
	            $shortcode = $_POST["shortcode"];
	            $status = $_POST["status"];
	            $timestamp = $_POST["timestamp"];
	            $credits_cost = $_POST["credits_cost"];
	            
	            echo "Accepted";
	            exit(0);
	        }
	        catch (Exception $e)
	        {
	            echo "Error";
	            exit(0);
	        }
	    }
	    else
	    {
	        echo "Error";
	        exit(0);
	    }

	}

	public function add_new_report(){
		if ($this->request->is('post')) {

			$mobile = null;
			$email = null;
			$this->loadModel('Informer');
			$this->loadModel('ElectricCooperative');

			$this->IME = $this->Components->load('IME');
			$EC = $this->ElectricCooperative->findById($this->request->data['TipOff']['electric_cooperatives_id']);
			$EC_code = $EC['ElectricCooperative']['code'];

			if(empty($this->request->data['TipOff']['address']) || empty($this->request->data['TipOff']['description']) || empty($this->request->data['TipOff']['electric_cooperatives_id']) ){

				$this->Session->setFlash(__('Address and description field are required.'), 'flash/warning');
				$this->redirect(array('controller' => 'pages','action' => 'index'));
			}	

			
				
				if(!empty($this->request->data['TipOff']['mobile'])){
					$mobile = $this->request->data['TipOff']['mobile'];
					if($informer = $this->Informer->findByMobile($mobile)){
						$this->Session->setFlash(__('Mobile number is already registered. Please login'), 'flash/warning');
						$this->redirect(array('controller' => 'users','action' => 'login'));
					} 
				}

				if(!empty($this->request->data['TipOff']['email'])){
					$email = $this->request->data['TipOff']['email'];
					if($informer = $this->Informer->findByEmail($email)){
						$this->Session->setFlash(__('Email is already registered. Please login'), 'flash/warning');
						$this->redirect(array('controller' => 'users','action' => 'login'));
					}
				}

				
				if($informerData = $this->IME->createInformer($mobile, $email, 'Reporter')){
					$informer = $this->Informer->findByCode($informerData['codename']);
					$informerPassword = $informerData['password'];
					$this->Session->write('Auth.User.id', $informer['Informer']['id']);
					$this->Session->write('Auth.User.username', $informer['Informer']['username']);
				}

				$continue = true;
				do {
					$tipOffCode = $EC_code.$this->IME->generateCode(3,5);
					if(!$this->TipOff->findByCode($tipOffCode)){
						$continue = false;
					}

				} while ($continue);

					
	  			$this->TipOff->create();	
				$this->request->data['TipOff']['code'] = trim($tipOffCode);
				$this->request->data['TipOff']['informer_id'] = $informer['Informer']['id'];
				$this->request->data['TipOff']['area_id'] = 0;
				$this->request->data['TipOff']['tip_off_status_id'] = 1; // For Submission
				$this->request->data['TipOff']['finding_id'] = 1; //None			
				$this->request->data['TipOff']['created'] = date('Y-m-d H:i:s');	
				$this->request->data['TipOff']['tip_off_type_id'] = 2;		  
				

				if ($this->TipOff->save($this->request->data)) {
					$reportID = $this->TipOff->getLastInsertID();

					$flashMessage = 'Thank you for your report.<br>
												Your report code is <b><span class="label label-info">'.$tipOffCode.'</span></b>.<br>
												Your codename is <b><span class="label label-danger">'.$informer['Informer']['username'].'</span></b> and password is <b><span class="label label-danger">'.$informerPassword.'</span></b>.<br>
												Please keep this for future reference.';
					$notifMessage = 'Thank you for your report. Your report code is '.$tipOffCode.'. Your codename is '.$informer['Informer']['username'].' and password is '.$informerPassword.'. Please keep this for future reference.';							

					$this->IME->notifyUser($informer['Informer']['id'], $notifMessage);
												
					$this->Session->setFlash(__($flashMessage), 'flash/success');
					$this->redirect(array('action' => 'view', $reportID));
				} else {
					$this->Session->setFlash(__('The tip off could not be saved. Please, try again.'), 'flash/danger');
				}				
		}

		$electricCooperatives = $this->TipOff->ElectricCooperative->find('list');
		$this->set(compact('electricCooperatives'));

	}
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TipOff->recursive = 0;
		$this->paginate = array('conditions' => array('TipOff.informer_id' => AuthComponent::User('id')));
		$this->set('tipOffs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TipOff->exists($id)) {
			throw new NotFoundException(__('Invalid tip off'));
		}


		if($this->isAuthorized(AuthComponent::User())){
			$options = array('conditions' => array('TipOff.' . $this->TipOff->primaryKey => $id));
			$this->set('tipOff', $this->TipOff->find('first', $options));
		} else {
			$this->redirect(array('action' => 'index'));
		}	
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {

	
			$mobile = null;
			$email = null;
			$this->loadModel('Informer');
			$this->loadModel('ElectricCooperative');

			$this->IME = $this->Components->load('IME');
			$EC = $this->ElectricCooperative->findById($this->request->data['TipOff']['electric_cooperatives_id']);
			$EC_code = $EC['ElectricCooperative']['code'];

			if(empty($this->request->data['TipOff']['address']) || empty($this->request->data['TipOff']['description']) || empty($this->request->data['TipOff']['electric_cooperatives_id']) ){

				$this->Session->setFlash(__('Address and description field are required.'), 'flash/warning');
				$this->redirect(array('controller' => 'pages','action' => 'index'));
			}	

			$continue = true;
			do {
				$tipOffCode = $EC_code.$this->IME->generateCode(3,5);
				if(!$this->TipOff->findByCode($tipOffCode)){
					$continue = false;
				}

			} while ($continue);

				$informer['Informer']['id'] = AuthComponent::User('id');
				
								
	  			$this->TipOff->create();	
				$this->request->data['TipOff']['code'] = trim($tipOffCode);
				$this->request->data['TipOff']['informer_id'] = $informer['Informer']['id'];
				$this->request->data['TipOff']['area_id'] = 0;
				$this->request->data['TipOff']['tip_off_status_id'] = 1; // For Submission
				$this->request->data['TipOff']['finding_id'] = 1; //None			
				$this->request->data['TipOff']['created'] = date('Y-m-d H:i:s');	
				$this->request->data['TipOff']['tip_off_type_id'] = 2;		  
				

				if ($this->TipOff->save($this->request->data)) {
					$reportID = $this->TipOff->getLastInsertID();
					$flashMessage = 'Thank you for your report.<br>
												Your report code is <b><span class="label label-info">'.$tipOffCode.'</span></b>.<br>
												Please keep this for future reference.';
					$notifMessage = 'Thank you for your report. Your report code is '.$tipOffCode.'. Please keep this for future reference.';
					$this->IME->notifyUser($informer['Informer']['id'], $notifMessage);

					$this->Informer->create();
					$toCount = array('id' => $informer['Informer']['id'], 'tip_offs_count' => $informer['Informer']['tip_offs_count'] + 1);
					$this->Informer->save($toCount);
												
					$this->Session->setFlash(__($flashMessage), 'flash/success');
					$this->redirect(array('action' => 'view', $reportID));
				} else {
					$this->Session->setFlash(__('The tip off could not be saved. Please, try again.'), 'flash/danger');
				}				
		} //else {
		// 	$this->redirect(array('action' => 'index'));
		// }

		$electricCooperatives = $this->TipOff->ElectricCooperative->find('list');
		$this->set(compact('electricCooperatives'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->TipOff->id = $id;
		if (!$this->TipOff->exists($id)) {
			throw new NotFoundException(__('Invalid tip off'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TipOff->save($this->request->data)) {
				$this->Session->setFlash(__('The tip off has been saved'), 'flash/success');
				$this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('The tip off could not be saved. Please, try again.'), 'flash/danger');
			}
		} else {
			$options = array('conditions' => array('TipOff.' . $this->TipOff->primaryKey => $id));
			$this->request->data = $this->TipOff->find('first', $options);
		}
		$informers = $this->TipOff->Informer->find('list');
		$electricCooperatives = $this->TipOff->ElectricCooperative->find('list');
		$tipOffStatuses = $this->TipOff->TipOffStatus->find('list');
		$findings = $this->TipOff->Finding->find('list');
		$areas = $this->TipOff->Area->find('list');
		$tipOffTypes = $this->TipOff->TipOffType->find('list');
		$this->set(compact('informers', 'electricCooperatives', 'tipOffStatuses', 'findings', 'areas', 'tipOffTypes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TipOff->id = $id;
		if (!$this->TipOff->exists()) {
			throw new NotFoundException(__('Invalid tip off'));
		}
		if ($this->TipOff->delete()) {
			$this->Session->setFlash(__('Tip off deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Tip off was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}



	public function android_add_new_report(){
		$this->layout = 'ajax';
		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag']) && !empty($_POST['electric_cooperatives_id'])) {

			if($_POST['tag'] == 'itsmyelectric_android_report') {

				$this->loadModel('ElectricCooperative');

				$this->IME = $this->Components->load('IME');
				$EC = $this->ElectricCooperative->findById($_POST['electric_cooperatives_id']);
				$EC_code = $EC['ElectricCooperative']['code'];

					
					$continue = true;
					do {
						$tipOffCode = $EC_code.$this->IME->generateCode(3,5);
						if(!$this->TipOff->findByCode($tipOffCode)){
							$continue = false;
						}

					} while ($continue);

					$today = date('Y-m-d H:i:s');

		  			$this->TipOff->create();	
					$this->request->data['TipOff']['code'] = trim($tipOffCode);
					$this->request->data['TipOff']['informer_id'] = $_POST['informer_id'];
					$this->request->data['TipOff']['electric_cooperatives_id'] = $EC['ElectricCooperative']['id'];
					$this->request->data['TipOff']['description'] = $_POST['description'];
					$this->request->data['TipOff']['address'] = $_POST['address'];
					$this->request->data['TipOff']['area_id'] = 0;
					$this->request->data['TipOff']['tip_off_status_id'] = 1; // For Submission
					$this->request->data['TipOff']['finding_id'] = 1; //None			
					$this->request->data['TipOff']['created'] = $today;	
					$this->request->data['TipOff']['tip_off_type_id'] = 2;		  
					

					if ($this->TipOff->save($this->request->data)) {

						$reportID = $this->TipOff->getLastInsertID();

						$notifMessage = 'Thank you for your report.';							

						$this->IME->notifyUser($_POST['informer_id'], $notifMessage);
													
						$response["success"] = 1;
						$response["success_msg"] = $notifMessage;
						$response["TipOff"]['id'] = $reportID ;
						$response["TipOff"]['created'] = $today;	
						$response["TipOff"]['code'] = trim($tipOffCode);
						//$response["TipOff"]['area'] = 'None';
						//$response["TipOff"]['status'] = 'Report Submitted';
						//$response["TipOff"]['finding'] = 'None'; 
			            
			            echo json_encode($response);
					} else {
						$response["success"] = 0;
			        	$response["error"] = 1;
	            		$response["error_msg"] = "Error occured!";
	            		echo json_encode($response);
					}	
				}
		}

	}



	public function android_report_list(){
		
		$this->layout = 'ajax';
		$this->render('android_add_new_report');

		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_report') {

				$informerId = $_POST['informer_id'];
				$ecId = $_POST['ec_id'];

				$this->loadModel('Informer');

					$this->TipOff->recursive = 1;
					if($responses = $this->TipOff->findAllByElectricCooperativesIdAndInformerId($ecId, $informerId)){
						//$data["success"] = 1;
						$data = array('success' => 1, 'Results' => $responses);
						
					}	else {
						$data = array('success' => null);
					}	
						
					echo json_encode($data);

			}
			
		}
		
	}	


	public function android_ec_list(){
		
		$this->layout = 'ajax';
		$this->autoRender = false;

		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_report') {

				

				$this->loadModel('ElectricCooperative');

					$this->ElectricCooperative->recursive = 1;
					if($responses = $this->ElectricCooperative->find('all', array('order' => array('ElectricCooperative.electric_cooperatives ASC')))) {
						//$data["success"] = 1;
						$data = array('success' => 1, 'Results' => $responses);
						
					}	else {
						$data = array('success' => null);
					}	
						
					echo json_encode($data);

			}
			
		}
		
	}		

	// public function smsSender($mobile_number = null, $message = null) {
	// 	$this->layout = 'ajax';
	// 	$this->autoRender = false;

	// 		$arr_post_body = array(
	// 	        "message_type" => "SEND",
	// 	        "mobile_number" => $mobile_number,
	// 	        "shortcode" => "29290999999",
	// 	        "message_id" => "12345789012345678901234567890122",
	// 	        "message" => urlencode($message),
	// 	        "client_id" => "a4a4cd6894d813c5d8201bb549f9d04ad67bede385b5be2b327570643e685e7c",
	// 	        "secret_key" => "dd929e43d159b5ce524de110b160b360eb0cbd6b7e24c8f14a7d1d8df94a766d"
	// 	    );

	// 	    $query_string = "";
	// 	    foreach($arr_post_body as $key => $frow)
	// 	    {
	// 	        $query_string .= '&'.$key.'='.$frow;
	// 	    }

	// 	    $URL = "https://post.chikka.com/smsapi/request";
	// 	    error_log("sms sender!!!");
	// 	    $curl_handler = curl_init();
	// 	    curl_setopt($curl_handler, CURLOPT_URL, $URL);
	// 	    curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
	// 	    curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
	// 	    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
	// 	    $response = curl_exec($curl_handler);
	// 	    curl_close($curl_handler);

	// 	    exit(0);
	// }

}	