<?php
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
/**
 * Chats Controller
 *
 * @property Chat $Chat
 * @property PaginatorComponent $Paginator
 */
class ChatsController extends AppController {

	public $components = array('Paginator', 'Security');

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('android_add', 'android_chat_list', 'android_send_email'));
		$this->Security->unlockedActions = array('android_add', 'android_chat_list', 'android_send_email');
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Chat->recursive = 0;
		$this->paginate = array('conditions' => array('Chat.informer_id' => AuthComponent::User('id')));
		$this->set('chats', $this->paginate());
		$this->loadModel('ElectricCooperative');
		$electricCooperatives = $this->ElectricCooperative->find('list', array('order'=> 'abbreviation asc'));
		$this->set(compact('electricCooperatives'));
	}

	public function chats($ec_id = null){
		if(isset($ec_id)){
			$this->paginate = array('conditions' => array('AND' => array('Chat.electric_cooperatives_id' => $ec_id), array('Chat.informer_id' => AuthComponent::User('id'))), 
									'order' => 'Chat.id DESC'
									);
		} else {
			$this->paginate = array('conditions' => array('Chat.informer_id' => AuthComponent::User('id')),
									'order' => 'Chat.id DESC');
		}
			$this->set('chats', $this->paginate());

	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Chat->exists($id)) {
			throw new NotFoundException(__('Invalid chat'));
		}
		$options = array('conditions' => array('Chat.' . $this->Chat->primaryKey => $id));
		$this->set('chat', $this->Chat->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Chat->create();
			$this->request->data['Chat']['date_created'] = date();
			$this->request->data['Chat']['type'] = 'in';
			$this->request->data['Chat']['informer_id'] = AuthComponent::User('id');
			if ($this->Chat->save($this->request->data)) {
				$this->Session->setFlash(__('The chat has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The chat could not be saved. Please, try again.'), 'flash/error');
			}
		}
		//$users = $this->Chat->User->find('list');
		//$informers = $this->Chat->Informer->find('list');
		$this->loadModel('ElectricCooperative');
		$electricCooperatives = $this->ElectricCooperative->find('list', array('order'=> 'abbreviation asc'));
		//$deliveryStatuses = $this->Chat->DeliveryStatus->find('list');
		$this->set(compact('electricCooperatives'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Chat->id = $id;
		if (!$this->Chat->exists($id)) {
			throw new NotFoundException(__('Invalid chat'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Chat->save($this->request->data)) {
				$this->Session->setFlash(__('The chat has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The chat could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Chat.' . $this->Chat->primaryKey => $id));
			$this->request->data = $this->Chat->find('first', $options);
		}
		$users = $this->Chat->User->find('list');
		$informers = $this->Chat->Informer->find('list');
		$electricCooperatives = $this->Chat->ElectricCooperative->find('list');
		$deliveryStatuses = $this->Chat->DeliveryStatus->find('list');
		$this->set(compact('users', 'informers', 'electricCooperatives', 'deliveryStatuses'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Chat->id = $id;
		if (!$this->Chat->exists()) {
			throw new NotFoundException(__('Invalid chat'));
		}
		if ($this->Chat->delete()) {
			$this->Session->setFlash(__('Chat deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Chat was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}



	/**
 * add method
 *
 * @return void
 */
	public function android_add() {

		$this->autoRender = false;
		$response["success"] = 0;

		if ($this->request->is('post') && !empty($_POST['tag']) && $_POST['tag'] == 'itsmyelectric_android_chat') {

			//$this->loadModel('Informer');
			// $EC = $this->Informer->find('first', array('conditions'=> array('Informer.id' => $_POST['informer_id']),
			// 											'fields' => array('Informer.electric_cooperatives_id')));

			$this->Chat->create();
			$this->request->data['Chat']['date_created'] = date('Y-m-d H:i:s');
			$this->request->data['Chat']['code'] = 'DEMO'; 
			$this->request->data['Chat']['informer_id'] = $_POST['informer_id'];
			$this->request->data['Chat']['message'] = $_POST['message'];
			$this->request->data['Chat']['electric_cooperatives_id'] = $_POST['ec_id'];
			$this->request->data['Chat']['type'] = 'in';
			if ($this->Chat->save($this->request->data)) {
				$response["success"] = 1;
			} 


		}
		echo json_encode($response);
	}


	public function android_chat_list(){
		
		$this->autoRender = false;

		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_chat') {

				$informerId = $_POST['informer_id'];

					$this->Chat->recursive = 1;
					if($responses = $this->Chat->findAllByInformerId($informerId)){
						//$data["success"] = 1;
						$data = array('success' => 1, 'Results' => $responses);
						
					}	else {
						$data = array('success' => null);
					}	
						
					echo json_encode($data);

			}
			
		}
		
	}	


	public function android_send_email(){

		$this->autoRender = false;
		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_email') {

				$this->loadModel('Informer');
				$informer = $this->Informer->find('first', array('conditions' => array('Informer.id' => $_POST['informer_id']),
													  'fields' => array('Informer.code')));

				$recipient = 'sherwinrobles@gmail.com';
				$sender = 'notification@itsmyelectric.org';
				$message = $_POST['message']."\r\n\r\n";
				$message .= $informer['Informer']['code'];
				$subject = $_POST['subject'];
				$Email = new CakeEmail();
				$Email->config('gmail');
				$Email->from(array($sender => 'ItsMyElectric.org'))
	    		->to($recipient)
	    		->subject('Email from Android | '.$subject)
	    		->send($message);

	    		$data = array('success' => 1);
						
			} else {
				$data = array('success' => null);
			}	
						
				echo json_encode($data);	
    	}	

	}





}
