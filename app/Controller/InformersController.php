<?php
App::uses('AppController', 'Controller');
/**
 * Informers Controller
 *
 * @property Informer $Informer
 * @property PaginatorComponent $Paginator
 */
class InformersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Informer->recursive = 0;
		$this->set('informers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Informer->exists($id)) {
			throw new NotFoundException(__('Invalid informer'));
		}
		$options = array('conditions' => array('Informer.' . $this->Informer->primaryKey => $id));
		$this->set('informer', $this->Informer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Informer->create();
			if ($this->Informer->save($this->request->data)) {
				$this->Session->setFlash(__('The informer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The informer could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Informer->exists($id)) {
			throw new NotFoundException(__('Invalid informer'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Informer->save($this->request->data)) {
				$this->Session->setFlash(__('The informer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The informer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Informer.' . $this->Informer->primaryKey => $id));
			$this->request->data = $this->Informer->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Informer->id = $id;
		if (!$this->Informer->exists()) {
			throw new NotFoundException(__('Invalid informer'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Informer->delete()) {
			$this->Session->setFlash(__('The informer has been deleted.'));
		} else {
			$this->Session->setFlash(__('The informer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
