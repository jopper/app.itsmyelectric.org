<?php
App::uses('AppController', 'Controller');
/**
 * Rewards Controller
 *
 * @property Reward $Reward
 * @property PaginatorComponent $Paginator
 */
class RewardsController extends AppController {

	public $components = array('Paginator', 'Security');

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('check_reward', 'insert_smartmoney'));
		$this->Security->unlockedActions = array('check_reward', 'insert_smartmoney');
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Reward->recursive = 0;
		$this->set('rewards', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Reward->exists($id)) {
			throw new NotFoundException(__('Invalid reward'));
		}
		$options = array('conditions' => array('Reward.' . $this->Reward->primaryKey => $id));
		$this->set('reward', $this->Reward->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Reward->create();
			if ($this->Reward->save($this->request->data)) {
				$this->Session->setFlash(__('The reward has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reward could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$users = $this->Reward->User->find('list');
		$tipOffs = $this->Reward->TipOff->find('list');
		$electricCooperatives = $this->Reward->ElectricCooperative->find('list');
		$paymentSchedules = $this->Reward->PaymentSchedule->find('list');
		$informers = $this->Reward->Informer->find('list');
		$rewardStatuses = $this->Reward->RewardStatus->find('list');
		$this->set(compact('users', 'tipOffs', 'electricCooperatives', 'paymentSchedules', 'informers', 'rewardStatuses'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Reward->id = $id;
		if (!$this->Reward->exists($id)) {
			throw new NotFoundException(__('Invalid reward'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Reward->save($this->request->data)) {
				$this->Session->setFlash(__('The reward has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The reward could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Reward.' . $this->Reward->primaryKey => $id));
			$this->request->data = $this->Reward->find('first', $options);
		}
		$users = $this->Reward->User->find('list');
		$tipOffs = $this->Reward->TipOff->find('list');
		$electricCooperatives = $this->Reward->ElectricCooperative->find('list');
		$paymentSchedules = $this->Reward->PaymentSchedule->find('list');
		$informers = $this->Reward->Informer->find('list');
		$rewardStatuses = $this->Reward->RewardStatus->find('list');
		$this->set(compact('users', 'tipOffs', 'electricCooperatives', 'paymentSchedules', 'informers', 'rewardStatuses'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Reward->id = $id;
		if (!$this->Reward->exists()) {
			throw new NotFoundException(__('Invalid reward'));
		}
		if ($this->Reward->delete()) {
			$this->Session->setFlash(__('Reward deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Reward was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}


	public function check_reward(){
		$this->autoRender = false;
		if($this->request->is('post') && isset($_POST['tag']) && $_POST['tag'] == 'android_reward'){
			$reportId = $_POST['report_id'];
			$rewards = $this->Reward->find('first', array('conditions' => array('AND' => array(
																				array('Reward.tip_off_id' => $reportId),
																				array('Reward.reward_status_id' => 1),
																				array('Reward.smartmoney_card_no' => '')
																				)),
															'fields' => array('Reward.id')
															));
			if($rewards){
				$result = array('success' => 1, 'Results' => $rewards);
			} else {
				$result = array('success' => 0);
			}
			
		} else {
			$result = array('success' => 0);
		}	

		echo json_encode($result);
	}

	public function insert_smartmoney(){
		$this->autoRender = false;
		if($this->request->is('post') && isset($_POST['tag']) && $_POST['tag'] == 'android_reward'){
			$rewardId = $_POST['reward_id'];
			$reward = $this->Reward->findById($rewardId);
			$reward['Reward']['smartmoney_card_no'] = $_POST['smartmoney_card_no'];
			$reward['Reward']['reward_status_id'] = 2;
			if($this->Reward->save($reward)){
				$result = array('success' => 1);
			} else {
				$result = array('success' => 0);
			}
		
		}  else {
			$result = array('success' => 0);
		}	

		echo json_encode($result);	

	}






}
