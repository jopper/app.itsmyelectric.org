<?php
App::uses('AppController', 'Controller');
/**
 * Feedbacks Controller
 *
 * @property Feedback $Feedback
 * @property PaginatorComponent $Paginator
 */
class FeedbacksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Security');

	public function beforeFilter(){
		parent::beforeFilter();
		$this->Auth->allow(array('android_add_feedback', 'android_feedback_list'));
		$this->Security->unlockedActions = array('android_add_feedback', 'android_feedback_list');
	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Feedback->recursive = 0;
		$this->paginate = array('order' => 'Feedback.id DESC');
		$this->set('feedbacks', $this->paginate());
		$this->loadModel('ElectricCooperative');
		$electricCooperatives = $this->ElectricCooperative->find('list', array('order'=> 'abbreviation asc'));
		$this->set(compact('electricCooperatives'));
	}

	public function feedbacks($ec_id = null){
		if(isset($ec_id)){
			$this->paginate = array('conditions' => array('Feedback.electric_cooperatives_id' => $ec_id), 'order' => 'Feedback.id DESC');
		} else {
			$this->paginate = array('order' => 'Feedback.id DESC');
		}
			$this->set('feedbacks', $this->paginate());

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
		$this->set('feedback', $this->Feedback->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Feedback->create();
			$this->request->data['Feedback']['created'] = date("Y-m-d H:i:s");
			$this->request->data['Feedback']['informer_id'] = AuthComponent::User('id');
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$this->loadModel('ElectricCooperative');
		$electricCooperatives = $this->ElectricCooperative->find('list', array('order'=> 'abbreviation asc'));
		$this->set(compact('electricCooperatives'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Feedback->id = $id;
		if (!$this->Feedback->exists($id)) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Feedback->save($this->request->data)) {
				$this->Session->setFlash(__('The feedback has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The feedback could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Feedback.' . $this->Feedback->primaryKey => $id));
			$this->request->data = $this->Feedback->find('first', $options);
		}
		$informers = $this->Feedback->Informer->find('list');
		$electricCooperatives = $this->Feedback->ElectricCooperative->find('list');
		$this->set(compact('informers', 'electricCooperatives'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Feedback->id = $id;
		if (!$this->Feedback->exists()) {
			throw new NotFoundException(__('Invalid feedback'));
		}
		if ($this->Feedback->delete()) {
			$this->Session->setFlash(__('Feedback deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Feedback was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}


/**
 * add method
 *
 * @return void
 */
	public function android_add_feedback() {
		$this->autoRender = false;
		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_feedback') {
				
				

				$informerId = $_POST['informer_id'];
				$feedbackType = $_POST['feedback_type'];
				$message = $_POST['message'];
				$ecId = $_POST['ec_id'];

				$this->loadModel('ElectricCooperative');
				$EC = $this->ElectricCooperative->findById($ecId);
				$ECPositive = $EC['ElectricCooperative']['positive_feedback'];
				$ECNegative = $EC['ElectricCooperative']['negative_feedback'];

				// $this->loadModel('Informer');
				// $informerData = $this->Informer->findById($informerId);
				// $infoPositiveCount = $informerData['Informer']['positive_feedback_count'];
				// $infoNegativeCount = $informerData['Informer']['negative_feedback_count'];
				

				$this->Feedback->create();
				$this->request->data = array('created' => date('Y-m-d H:i:s'), 
											'informer_id' => $informerId,
											'type' => $feedbackType,
											'electric_cooperatives_id' => $ecId,
											'message' => $message );

				if ($this->Feedback->save($this->request->data)) {
					$response["success"] = 1;

					$this->ElectricCooperative->create();
					if($feedbackType == '[ + ]'){
						$ecFeed = array('id' => $ecId,
										'positive_feedback' => $ECPositive+1);
					} else {
						$ecFeed = array('id' => $ecId,
										'negative_feedback' => $ECNegative+1);
					}
					$this->ElectricCooperative->save($ecFeed);

					
				} else {
					$response["success"] = 0;
	            		
				}

				echo json_encode($response);
			}

		}
		
	}

	public function android_feedback_list(){

		$this->autoRender = false;

		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

			if($_POST['tag'] == 'itsmyelectric_android_feedback') {

				$informerId = $_POST['informer_id'];
				$ecId = $_POST['ec_id'];

				// $this->loadModel('Informer');
				// $informerData = $this->Informer->findById($informerId);

				$this->loadModel('TipOff');
				if($tipOffData = $this->TipOff->findByElectricCooperativesIdAndInformerId($ecId, $informerId)){
					$responses = $this->Feedback->query("Select Feedback.*, b.pos_cnt, b.neg_cnt, TipOff.total, Informer.username, ElectricCooperative.abbreviation from(
															Select * from feedbacks WHERE electric_cooperatives_id='".$ecId."' order by id desc )Feedback,
															(select informer_id, electric_cooperatives_id, sum(case when type='[ - ]' then 1 else 0 end)neg_cnt, sum(case when type='[ + ]' then 1 else 0 end)pos_cnt from feedbacks group by informer_id,electric_cooperatives_id)b,
															(select informer_id, electric_cooperatives_id, count(*) total from tip_offs group by informer_id,electric_cooperatives_id)TipOff,
															(select id, username from informers)Informer,
															(select id, abbreviation from electric_cooperatives)ElectricCooperative
															Where Feedback.informer_id=b.informer_id
															And Feedback.informer_id=TipOff.informer_id
															And Feedback.informer_id=Informer.id
															And Feedback.electric_cooperatives_id=ElectricCooperative.id
															And Feedback.electric_cooperatives_id=b.electric_cooperatives_id
															And Feedback.electric_cooperatives_id=TipOff.electric_cooperatives_id ORDER BY id desc");
				} else {
					$responses = $this->Feedback->query("Select Feedback.*, b.pos_cnt, b.neg_cnt, Informer.username, ElectricCooperative.abbreviation from(
															Select * from feedbacks WHERE electric_cooperatives_id='".$ecId."' order by id desc )Feedback,
															(select informer_id, electric_cooperatives_id, sum(case when type='[ - ]' then 1 else 0 end)neg_cnt, sum(case when type='[ + ]' then 1 else 0 end)pos_cnt from feedbacks group by informer_id,electric_cooperatives_id)b,
															(select id, username from informers)Informer,
															(select id, abbreviation from electric_cooperatives)ElectricCooperative
															Where Feedback.informer_id=b.informer_id
															And Feedback.informer_id=Informer.id
															And Feedback.electric_cooperatives_id=ElectricCooperative.id
															And Feedback.electric_cooperatives_id=b.electric_cooperatives_id ORDER BY id desc");
				}

					//$this->Feedback->recursive = 1;
					if($responses){
						$data = array('success' => 1, 'Results' => $responses);
						
					} else {
					 	$data = array('success' => null);
					}	
						
					echo json_encode($data);

			}
			
		}
		
	}	




}
