<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Security');


	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow(array('android_login', 'android_register', 'android_user_stats')); // Letting users register themselves
	    $this->Security->unlockedActions = array('android_login', 'android_register', 'android_user_stats');
	}


	
	public function login() {
		$this->layout = 'login';
	    if ($this->request->is('post')) {
	        if ($this->Auth->login()) {
	            return $this->redirect($this->Auth->redirect());
	        }
	        $this->Session->setFlash(__('Invalid username or password, try again'), 'flash/danger');
	    }
	}


	public function logout() {
	    return $this->redirect($this->Auth->logout());
	}


	public function update_contacts($id = null) {

		if (AuthComponent::User('id') != $id){
			$this->redirect(array('action' => 'view', AuthComponent::User('id')));
		}

        $this->User->id = $id;
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}

		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('Your contacts has been saved'), 'flash/success');
				$this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('Your contacts can not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

	public function change_password($id = null) {

		if (AuthComponent::User('id') != $id){
			$this->redirect(array('action' => 'view', AuthComponent::User('id')));
		}

        $this->User->id = $id;
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {

			if(empty($this->request->data['User']['current_password']) || empty($this->request->data['User']['new_password']) || empty($this->request->data['User']['confirm_password'])){
				$this->Session->setFlash(__('All fields required'), 'flash/danger');
				$this->redirect(array('action' => 'change_password', $id));
			}

			if(AuthComponent::password(trim($this->request->data['User']['current_password'])) != $this->request->data['User']['password']){
				$this->Session->setFlash(__('Current password does not match.'), 'flash/danger');
				$this->redirect(array('action' => 'change_password', $id));
			}

			if($this->request->data['User']['new_password'] != $this->request->data['User']['confirm_password']){
				$this->Session->setFlash(__('New password does not match with confirm password.'), 'flash/danger');
				$this->redirect(array('action' => 'change_password', $id));
			}
			$this->request->data['User']['password'] = trim($this->request->data['User']['new_password']);
			if ($this->User->save($this->request->data)) {

				$this->Session->setFlash(__('Your password has been saved'), 'flash/success');
				$this->redirect(array('action' => 'view', $id));
			} else {
				$this->Session->setFlash(__('Your password not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (AuthComponent::User('id') != $id){
			$this->redirect(array('action' => 'view', AuthComponent::User('id')));
		}

		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (AuthComponent::User('id') != $id){
			$this->redirect(array('action' => 'view', AuthComponent::User('id')));
		}

        $this->User->id = $id;
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		if (AuthComponent::User('id') != $id){
			$this->redirect(array('action' => 'view', AuthComponent::User('id')));
		}

		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}


	public function android_login() {
		$this->layout = 'ajax';
	    if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

	    	if($_POST['tag'] == 'itsmyelectric_android_login') {
	    		$username = $_POST['username'];
       			$password = $_POST['password'];

       			if(empty($username) || empty($password)) {
       				$response["success"] = 0;
			        $response["error"] = 1;
	            	$response["error_msg"] = "Codename and password is required. Please try again.";
	            	echo json_encode($response);
       			} else {
			    	$this->request->data['User']['username'] = $username;
			    	$this->request->data['User']['password'] = $password;
			        if ($this->Auth->login()) {
			            $response["success"] = 1;
			            $response["user"]["uid"] = AuthComponent::User('id');
			            $response["user"]["username"] = AuthComponent::User('username');
			            $response["user"]["email"] = AuthComponent::User('email');
			            $response["user"]["mobile"] = AuthComponent::User('mobile');
			            $response["user"]["created"] = AuthComponent::User('created');
			            echo json_encode($response);
			        } else {
			        	$response["success"] = 0;
			        	$response["error"] = 1;
	            		$response["error_msg"] = "Incorrect codename or password. Please try again.";
	            		echo json_encode($response);

			        }
			    }
		    }    
	    }
	}

	public function android_register() {

		$this->layout = 'ajax';
		$this->render('android_login');

	    if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

	    	if($_POST['tag'] == 'itsmyelectric_android_register') {


	    		$username = $_POST['username'];
       			$password = $_POST['password'];
       			$email = null;
       			$mobile = null;
       			//$electricCoopId = $_POST['electric_cooperatives_id'];
       			$response["error"] = 0;
       			$response["success"] = 0;
       			$this->loadModel('Informer');
       			$this->IME = $this->Components->load('IME');

       			if(empty($_POST['username']) || empty($_POST['password'])){
       				$response["error"] = 1;
	            	$response["error_msg"] = "Codename and password is required. Please try again.";
	            	echo json_encode($response);
       			}


       			if($informer = $this->Informer->findByUsername($username)){
			        		$response["error"] = 1;
	            			$response["error_msg"] = "Codename ".$username." is not available. Please try again.";
	            			echo json_encode($response);
				} 

       			if(!empty($_POS['mobile'])){
					$mobile = $_POS['mobile'];
					if($informer = $this->Informer->findByMobile($mobile)){
		        		$response["error"] = 1;
            			$response["error_msg"] = "Mobile number is already registered. Please login";
            			echo json_encode($response);
					} 
				}

				if(!empty($_POS['email'])){
					$email = $_POS['email'];
					if($informer = $this->Informer->findByEmail($email)){
		        		$response["error"] = 1;
            			$response["error_msg"] = "Email is already registered. Please login";
            			echo json_encode($response);
					}
				}


				if($response["error"] == 0){					
					if($informerData = $this->IME->createInformer($mobile, $email, 'Reporter', $username, $password)){
						$informer = $this->Informer->findByCode($informerData['codename']);

						$notifMessage = 'Thank you for registering.';							

						//$this->IME->notifyUser($informer['Informer']['id'], $notifMessage);

						$response["success"] = 1;
						$response["success_msg"] = $notifMessage;
			            $response["user"]["uid"] = $informer['Informer']['id'];
			            $response["user"]["username"] = $informer['Informer']['username'];
			            $response["user"]["email"] = $informer['Informer']['email'];
			            $response["user"]["mobile"] = $informer['Informer']['mobile'];
			            $response["user"]["created"] = $informer['Informer']['created'];
			            echo json_encode($response);
					}
				}


	    	}	
	    }

	}	


	public function android_user_stats() {

		$this->autoRender = false;

		if ($this->request->is('post') && isset($_POST['tag']) && !empty($_POST['tag'])) {

	    	if($_POST['tag'] == 'android_user_stats') {

	    		$informerId = $_POST['informer_id'];

	    		$this->loadModel('Feedback');
	    		//$this->load('Feedback');
	    		if($responses = $this->Feedback->query("SELECT Informer.username, ElectricCooperative.abbreviation, Summary_Count.pos_cnt, Summary_Count.neg_cnt, Summary_Count.total FROM
														(SELECT id, username FROM informers)Informer,
														(SELECT id, abbreviation FROM electric_cooperatives ORDER BY abbreviation DESC)ElectricCooperative,
														    (SELECT informer_id, electric_cooperatives_id, SUM(neg_cnt) neg_cnt, SUM(pos_cnt) pos_cnt, SUM(total) total FROM(
														(SELECT informer_id, electric_cooperatives_id,0 AS neg_cnt, 0 AS pos_cnt, COUNT(*) total FROM tip_offs GROUP BY informer_id, electric_cooperatives_id)
														UNION
														(SELECT informer_id, electric_cooperatives_id, SUM(CASE WHEN TYPE='[ - ]' THEN 1 ELSE 0 END)neg_cnt, SUM(CASE WHEN TYPE='[ + ]' THEN 1 ELSE 0 END)pos_cnt, 0 AS total
														FROM feedbacks GROUP BY informer_id,electric_cooperatives_id)
														)tbl_union
														GROUP BY informer_id, electric_cooperatives_id)Summary_Count
														WHERE Informer.id=Summary_Count.informer_id
														AND Summary_Count.electric_cooperatives_id=ElectricCooperative.id
														AND Informer.id='".$informerId."'")){

					$data = array('success' => 1, 'Results' => $responses);
						
				} else {
				 	$data = array('success' => null);
				}	
						
					echo json_encode($data);

	    	}

	    }	

	}	



}
