<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

	 public $components = array('Security');
/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();

	public function beforeFilter() {
	    parent::beforeFilter();
	    $this->Auth->allow('index', 'contact_us'); // Letting users register themselves
	}

/**
 * Displays a view
 *
 * @param mixed What page to display
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

	public function index() {

		$this->loadModel('ElectricCooperative');
		$electricCooperatives = $this->ElectricCooperative->find('list', array('order' => array('ElectricCooperative.abbreviation asc')));
		$this->set(compact('electricCooperatives'));

	}

	public function contact_us(){
		if ($this->request->is('post')) {

			$message = "";
			$subject = "";
			$error = 0;

			if(AuthComponent::User()){
				$this->request->data['Page']['email'] = AuthComponent::User('email');
				$this->request->data['Page']['mobile'] = AuthComponent::User('mobile');
				$message .= "Reporter : ".AuthComponent::User('code').". ";
			}

			if($this->request->data['Page']['email']){
				$message .= "Email : ".$this->request->data['Page']['email'].". ";
			}
			if($this->request->data['Page']['mobile']){
				$message .= "Mobile : ".$this->request->data['Page']['mobile'].". ";
			}

			if($this->request->data['Page']['subject']){
				$subject = $this->request->data['Page']['subject'];
			} else {
				$this->Session->setFlash(__("Subject is required"), 'flash/error');
				$error = 1;
			}

			if($this->request->data['Page']['message']){
				$message .= $this->request->data['Page']['message'];
			} else {
				$this->Session->setFlash(__("Message is required"), 'flash/error');
				$error = 1;
			}

			if($error == 0){
				$Email = new CakeEmail();
				$Email->config('gmail');
				if($Email->from(array('info@itsmyelectric.org' => 'app.itsmyelectric.org'))
	    		->to('sherwinrobles@gmail.com')
	    		->subject($subject)
	    		->send($message)) {
	    			$this->Session->setFlash(__("Thank you. Your message has been sent."), 'flash/success');
	    			$this->redirect(array('controller' => 'contact_us'));
	    		} else {
	    			$this->Session->setFlash(__("Your message has not been sent. Please try again later."), 'flash/error');
	    			$this->redirect(array('controller' => 'contact_us'));
	    		}
	    	}	
    	}
	}


	// public function display() {
	// 	$path = func_get_args();

	// 	$count = count($path);
	// 	if (!$count) {
	// 		return $this->redirect('/');
	// 	}
	// 	$page = $subpage = $title_for_layout = null;

	// 	if (!empty($path[0])) {
	// 		$page = $path[0];
	// 	}
	// 	if (!empty($path[1])) {
	// 		$subpage = $path[1];
	// 	}
	// 	if (!empty($path[$count - 1])) {
	// 		$title_for_layout = Inflector::humanize($path[$count - 1]);
	// 	}
	// 	$this->set(compact('page', 'subpage', 'title_for_layout'));

	// 	try {
	// 		$this->render(implode('/', $path));
	// 	} catch (MissingViewException $e) {
	// 		if (Configure::read('debug')) {
	// 			throw $e;
	// 		}
	// 		throw new NotFoundException();
	// 	}
	// }
}
