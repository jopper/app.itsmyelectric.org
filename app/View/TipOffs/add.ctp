
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('My Reports'), array('action' => 'index')); ?></li>
				<!-- <li class="list-group-item"><?php echo $this->Html->link(__('List Tip Off Comments'), array('controller' => 'tip_off_comments', 'action' => 'index')); ?> </li> -->
				
			</ul><!-- /.list-group -->
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="tipOffs form">
		
			<?php echo $this->Form->create('TipOff', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Add Report'); ?></h2>
			
					<div class="form-group">
						<?php echo $this->Form->label('electric_cooperatives_id', 'Electric Cooperative');?>
							<?php echo $this->Form->input('electric_cooperatives_id', array('class' => 'form-control', 'required' => 'required', 'empty' => 'Please select')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('description', 'Description');?>
							<?php echo $this->Form->input('description', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('address', 'Address');?>
							<?php echo $this->Form->input('address', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->


				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->


<?php echo $this->Html->script(array('libs/jquery.validate.min', 'customs/validations.js')); ?>