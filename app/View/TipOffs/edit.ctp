
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">						
				<li class="list-group-item"><?php echo $this->Html->link(__('My Reports'), array('action' => 'index')); ?></li>
			</ul><!-- /.list-group -->
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="tipOffs form">
		
			<?php echo $this->Form->create('TipOff', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Edit Report'); ?></h2>
					<div class="form-group">
							<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('code', 'Code');?>
							<?php echo $this->Form->input('code', array('class' => 'form-control', 'disabled' => 'disabled')); ?>
					</div><!-- .form-group -->


					<div class="form-group">
						<?php echo $this->Form->label('electric_cooperatives_id', 'Electric Cooperatives');?>
							<?php echo $this->Form->input('electric_cooperatives_id', array('class' => 'form-control', 'disabled' => 'disabled')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('description', 'Description');?>
							<?php echo $this->Form->input('description', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('address', 'Address');?>
							<?php echo $this->Form->input('address', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->


				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
