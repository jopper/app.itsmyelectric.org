
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('New Report'), array('action' => 'add'), array('class' => '')); ?></li>						
				
		<!-- <li class="list-group-item"><?php echo $this->Html->link(__('List Tip Off Comments'), array('controller' => 'tip_off_comments', 'action' => 'index'), array('class' => '')); ?></li>  -->
		<!-- <li class="list-group-item"><?php echo $this->Html->link(__('New Comment'), array('controller' => 'tip_off_comments', 'action' => 'add'), array('class' => '')); ?></li> 
			</ul> --><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="tipOffs index">
		
			<h2><?php echo __('Reports'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						<tr>
															<!-- <th><?php echo $this->Paginator->sort('id'); ?></th>
															<th><?php echo $this->Paginator->sort('created'); ?></th> -->
															<th><?php echo $this->Paginator->sort('code'); ?></th>
															<th><?php echo $this->Paginator->sort('electric_cooperatives_id'); ?></th>
															<!-- <th><?php echo $this->Paginator->sort('description'); ?></th>
															<th><?php echo $this->Paginator->sort('address'); ?></th> -->
															<th><?php echo $this->Paginator->sort('tip_off_status_id'); ?></th>
															<th><?php echo $this->Paginator->sort('finding_id'); ?></th>
															<!-- <th><?php echo $this->Paginator->sort('area_id'); ?></th> -->
															<!-- <th><?php echo $this->Paginator->sort('date_assigned'); ?></th>
															<th><?php echo $this->Paginator->sort('date_confirmed'); ?></th>
															<th><?php echo $this->Paginator->sort('date_for_settlement'); ?></th>
															<th><?php echo $this->Paginator->sort('date_settled'); ?></th> -->
															<th><?php echo $this->Paginator->sort('tip_off_type_id', 'Reported Via'); ?></th>
															<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($tipOffs as $tipOff): ?>
	<tr>
		<!-- <td><?php echo h($tipOff['TipOff']['id']); ?>&nbsp;</td>
		<td><?php echo h($tipOff['TipOff']['created']); ?>&nbsp;</td> -->
		<td><?php echo h($tipOff['TipOff']['code']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($tipOff['ElectricCooperative']['abbreviation'], array('controller' => 'electric_cooperatives', 'action' => 'view', $tipOff['ElectricCooperative']['id'])); ?>
		</td>
		<!-- <td><?php echo h($tipOff['TipOff']['description']); ?>&nbsp;</td>
		<td><?php echo h($tipOff['TipOff']['address']); ?>&nbsp;</td> -->
		<td>
			<?php echo $tipOff['TipOffStatus']['status']; ?>
		</td>
		<td>
			<?php echo $tipOff['Finding']['findings']; ?>
		</td>
		<!-- <td>
			<?php echo $this->Html->link($tipOff['Area']['area'], array('controller' => 'areas', 'action' => 'view', $tipOff['Area']['id'])); ?>
		</td> -->
		<!-- <td><?php echo h($tipOff['TipOff']['date_assigned']); ?>&nbsp;</td>
		<td><?php echo h($tipOff['TipOff']['date_confirmed']); ?>&nbsp;</td>
		<td><?php echo h($tipOff['TipOff']['date_for_settlement']); ?>&nbsp;</td>
		<td><?php echo h($tipOff['TipOff']['date_settled']); ?>&nbsp;</td> -->
		<td>
			<?php echo $tipOff['TipOffType']['type']; ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $tipOff['TipOff']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tipOff['TipOff']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tipOff['TipOff']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $tipOff['TipOff']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
