
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">	
				<li class="list-group-item"><?php echo $this->Html->link(__('Add Evidence'), '#TipOffEvidenceAddForm'); ?> </li>		
				<li class="list-group-item"><?php echo $this->Html->link(__('Edit Report'), array('action' => 'edit', $tipOff['TipOff']['id']), array('class' => '')); ?> </li>
				<!-- <li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Tip Off'), array('action' => 'delete', $tipOff['TipOff']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $tipOff['TipOff']['id'])); ?> </li> -->
				<li class="list-group-item"><?php echo $this->Html->link(__('My Reports'), array('action' => 'index'), array('class' => '')); ?> </li>
				<li class="list-group-item"><?php echo $this->Html->link(__('New Report'), array('action' => 'add'), array('class' => '')); ?> </li>
				
				<!-- <li class="list-group-item"><?php echo $this->Html->link(__('List Tip Off Comments'), array('controller' => 'tip_off_comments', 'action' => 'index'), array('class' => '')); ?> </li> -->
				<!-- <li class="list-group-item"><?php echo $this->Html->link(__('New Comment'), array('controller' => 'tip_off_comments', 'action' => 'add'), array('class' => '')); ?> </li> -->
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="tipOffs view">

			<h2><?php  echo __('Report'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>		
							<td><strong><?php echo __('Id'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['id']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Code'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['code']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Electric Cooperatives'); ?></strong></td>
							<td>
								<?php echo $this->Html->link($tipOff['ElectricCooperative']['abbreviation'], array('controller' => 'electric_cooperatives', 'action' => 'view', $tipOff['ElectricCooperative']['id']), array('class' => '')); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Description'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['description']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Address'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['address']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Report Status'); ?></strong></td>
							<td>
								<?php echo $tipOff['TipOffStatus']['status']; ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Finding'); ?></strong></td>
							<td>
								<?php echo $tipOff['Finding']['findings']; ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Area'); ?></strong></td>
							<td>
								<?php echo $this->Html->link($tipOff['Area']['area'], array('controller' => 'areas', 'action' => 'view', $tipOff['Area']['id']), array('class' => '')); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Reported Via'); ?></strong></td>
							<td>
								<?php echo $tipOff['TipOffType']['type']; ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Date Assigned'); ?></strong></td>
							<td>
								<?php if($tipOff['TipOff']['date_assigned']){ echo h($tipOff['TipOff']['date_assigned']); } ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Date Confirmed'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['date_confirmed']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Date For Settlement'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['date_for_settlement']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Date Settled'); ?></strong></td>
							<td>
								<?php echo h($tipOff['TipOff']['date_settled']); ?>
								&nbsp;
							</td>
						</tr>					
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->


		<div class="col-sm-9">
		
			<?php echo $this->Form->create('TipOffEvidence', array('action' => 'add', 'inputDefaults' => array('label' => false), 'class' => 'form form-horizontal', 'type' => 'file')); ?>
					<fieldset>
						<h2><?php echo __('Evidence'); ?></h2>
					<div class="form-row control-group row-fluid">
						<div class="controls span9">
							<?php echo $this->Form->input('tip_off_id', array('value' => $tipOff['TipOff']['id'], 'type' => 'hidden', 'class' => 'span12')); ?>
							<?php echo $this->Form->input('tip_off_code', array('value' => $tipOff['TipOff']['code'], 'type' => 'hidden', 'class' => 'span12')); ?>
						</div><!-- .controls -->
					</div><!-- .control-group -->
					 <div class="form-row control-group row-fluid">
						<?php echo $this->Form->label('file_name', 'Upload', array('class' => 'control-label'));?>
						<div class="controls span9">
							<?php echo $this->Form->input('file_name', array('type' => 'file', 'class' => 'span12')); ?>
							<?php //echo $this->Form->file('Document.submittedfile'); ?>
						</div><!-- .controls -->
					</div><!-- .control-group -->
					<div class="form-row control-group row-fluid">
						<?php echo $this->Form->label('details', 'Details', array('class' => 'control-label'));?>
						<div class="controls span9">
							<?php echo $this->Form->input('details', array('class' => 'form-control')); ?>
							</div><!-- .controls -->
					</div><!-- .form-group -->
					<br>
	             	<div class="form-row control-group row-fluid">
	                  	<div class="controls span9 pull-right">
	                    	<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
	                 	</div>
	                </div>

					</fieldset>
				
				<?php echo $this->Form->end(); ?>
		</div>

			<div class="related col-sm-9">

				<h3><?php echo __('Evidence'); ?></h3>
				
				<?php if (!empty($tipOff['TipOffEvidence'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th><?php echo __(''); ?></th>
									<th><?php echo __('Submitted'); ?></th>
									<th><?php echo __('Details'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($tipOff['TipOffEvidence'] as $tipOffEvidence): ?>
										<tr>
											<td><?php if($tipOffEvidence['new_file_name']){ echo $this->Image->resize($tipOffEvidence['new_file_name'], 100, 100); } ?></td>
											<td><?php echo $tipOffEvidence['created']; ?></td>
											<td><?php echo $tipOffEvidence['details']; ?></td>
											<td class="actions">
												<?php echo $this->Html->link(__('View'), array('controller' => 'tip_off_evidences', 'action' => 'view', $tipOffEvidence['id']), array('class' => 'btn btn-default btn-xs')); ?>
												<?php echo $this->Html->link(__('Edit'), array('controller' => 'tip_off_evidences', 'action' => 'edit', $tipOffEvidence['id']), array('class' => 'btn btn-default btn-xs')); ?>
												<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tip_off_evidences', 'action' => 'delete', $tipOffEvidence['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $tipOffEvidence['id'])); ?>
											</td>
										</tr>
									<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<!-- <div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Evidence'), array('controller' => 'tip_off_evidences', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				
				</div> --><!-- /.actions -->
				
			</div><!-- /.related -->

					
			
					
						

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
