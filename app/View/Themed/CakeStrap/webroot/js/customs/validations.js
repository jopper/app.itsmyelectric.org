$(document).ready(function() {

		jQuery.validator.addMethod("mobilePH", function(value, element) {
			return this.optional(element) || /^(((\s|\s?\-\s?)?)639(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9]){8}$/.test(value);
		}, "Please specify a valid mobile number.");

		$("#TipOffAddNewReportForm").validate({
			    rules: {
			      'data[TipOff][electric_cooperatives_id]': {
													    	required: true,
													    },
			      'data[TipOff][description]': {
													    	required: true,
													    },
			      'data[TipOff][address]': {
													       	required: true,
													     },
			      'data[TipOff][email]': {
													        email: true,
													      },
			       'data[TipOff][mobile]': {													    
													        //number: true,
													        mobilePH: true 
													      },								      								      
			      
			    },
			    messages: {
			    	'data[TipOff][electric_cooperatives_id]': "Please select Electric Cooperatives.",
			    	'data[TipOff][address]': "Please input address.",
			      	'data[TipOff][description]': "Please input description.",
			      	'data[TipOff][email]': "Please enter a valid email address",
			      	'data[TipOff][mobile]': {
			        	mobilePH: "Please enter a valid mobile number. Follow format starts with 639 and must be 12 digits",
			        	//number: "Please input numbers only"
			      	}
			    }
		});

		
		$("#TipOffAddForm").validate({
			    rules: {
			      'data[TipOff][electric_cooperatives_id]': {
													    	required: true,
													    },
			      'data[TipOff][description]': {
													    	required: true,
													    },
			      'data[TipOff][address]': {
													       	required: true,
													     },								      								      
			      
			    },
			    messages: {
			    	'data[TipOff][electric_cooperatives_id]': "Please select Electric Cooperatives.",
			    	'data[TipOff][address]': "Please input address.",
			      	'data[TipOff][description]': "Please input description."
			    }
		});


		$("#PageContactUsForm").validate({
			    rules: {
			    	'data[Page][subject]': { required: true, },
			      	'data[Page][message]': { required: true, },
			      	'data[Page][email]': { email: true, },
			      	'data[Page][mobile]': { mobilePH: true },								      								      
			      
			    },
			    messages: {
			    	'data[Page][subject]': "Subject is required.",
			      	'data[Page][message]': "Message is required.",
			      	'data[Page][email]': "Please enter a valid email address",
			      	'data[Page][mobile]': {
			        	mobilePH: "Please enter a valid mobile number. Follow format starts with 639 and must be 12 digits",
			        	//number: "Please input numbers only"
			      	}
			    }
		});



	});