<div class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <a href="../" class="navbar-brand"></span> itsmyelectric.org</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
            <li>
              <?php echo $this->Html->link('<span class="glyphicon glyphicon-new-window"></span> Visit website', FULL_BASE_URL.'/itsmyelectric.org', array('escape' => false)); ?>
            </li>
            <li>
				<?php if(AuthComponent::User()){ echo $this->Html->link('Report', array('controller' => 'tip_offs', 'action' => 'add')); } else { echo $this->Html->link('Report', '/'); } ?>
            </li>
            <li>
              <?php echo $this->Html->link('Feedbacks', array('controller'=>'feedbacks', 'action'=>'index')); ?>
            </li>
            <li>
              <?php echo $this->Html->link('Hotline', array('controller'=>'chats', 'action'=>'index')); ?>
            </li>
            <li>
              <?php echo $this->Html->link('Contact Us', array('controller'=>'contact_us')); ?>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-right">
          	<?php if(!AuthComponent::User()){ ?>
			<li><?php echo $this->Html->link('Login', array('controller'=>'users', 'action'=>'login')); ?></li>
			<?php } else { ?>
		    <li class="dropdown">
		        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php if(AuthComponent::User('first_name')){ echo AuthComponent::User('first_name'); } 
		        else { echo AuthComponent::User('username'); } ?> <b class="caret"></b></a>
		        <ul class="dropdown-menu">
		          <li><?php echo $this->Html->link("Profile", array('controller'=>'users', 'action'=>'view', AuthComponent::User('id'))); ?></li>
		          <li><?php echo $this->Html->link("My Tip-Offs", array('controller'=>'tip_offs', 'action'=>'index')); ?></li>
		          <li><?php echo $this->Html->link('Logout', array('controller'=>'users', 'action'=>'logout')); ?></li>
		        </ul>
		    </li>
		    <?php } ?>
          </ul>

        </div>
      </div>
    </div>

