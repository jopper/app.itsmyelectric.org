
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">			
						<li class="list-group-item"><?php echo $this->Html->link(__('Edit Informer'), array('action' => 'edit', $informer['Informer']['id']), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Informer'), array('action' => 'delete', $informer['Informer']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $informer['Informer']['id'])); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Informers'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Informer'), array('action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Chats'), array('controller' => 'chats', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Chat'), array('controller' => 'chats', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Inspection Reports'), array('controller' => 'inspection_reports', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Inspection Report'), array('controller' => 'inspection_reports', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Rewards'), array('controller' => 'rewards', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Reward'), array('controller' => 'rewards', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Tip Offs'), array('controller' => 'tip_offs', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Tip Off'), array('controller' => 'tip_offs', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="informers view">

			<h2><?php  echo __('Informer'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Code'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['code']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Type'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['type']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Mobile'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['mobile']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Email'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['email']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Password'); ?></strong></td>
		<td>
			<?php echo h($informer['Informer']['password']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

					
			<div class="related">

				<h3><?php echo __('Related Chats'); ?></h3>
				
				<?php if (!empty($informer['Chat'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
											<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Date Created'); ?></th>
		<th><?php echo __('Date Sent'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Mobile No'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Informer Id'); ?></th>
		<th><?php echo __('Message'); ?></th>
		<th><?php echo __('Electric Cooperatives Id'); ?></th>
		<th><?php echo __('Type'); ?></th>
		<th><?php echo __('Processed'); ?></th>
		<th><?php echo __('Delivery Status Id'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($informer['Chat'] as $chat): ?>
		<tr>
			<td><?php echo $chat['id']; ?></td>
			<td><?php echo $chat['date_created']; ?></td>
			<td><?php echo $chat['date_sent']; ?></td>
			<td><?php echo $chat['code']; ?></td>
			<td><?php echo $chat['mobile_no']; ?></td>
			<td><?php echo $chat['user_id']; ?></td>
			<td><?php echo $chat['informer_id']; ?></td>
			<td><?php echo $chat['message']; ?></td>
			<td><?php echo $chat['electric_cooperatives_id']; ?></td>
			<td><?php echo $chat['type']; ?></td>
			<td><?php echo $chat['processed']; ?></td>
			<td><?php echo $chat['delivery_status_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'chats', 'action' => 'view', $chat['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'chats', 'action' => 'edit', $chat['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'chats', 'action' => 'delete', $chat['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $chat['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Chat'), array('controller' => 'chats', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
				
			</div><!-- /.related -->

					
			<div class="related">

				<h3><?php echo __('Related Inspection Reports'); ?></h3>
				
				<?php if (!empty($informer['InspectionReport'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
											<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Inspection Date'); ?></th>
		<th><?php echo __('Tip Off Id'); ?></th>
		<th><?php echo __('Electric Cooperatives Id'); ?></th>
		<th><?php echo __('Consumer Name'); ?></th>
		<th><?php echo __('Representative'); ?></th>
		<th><?php echo __('Current Address'); ?></th>
		<th><?php echo __('Billing Address'); ?></th>
		<th><?php echo __('Meter No'); ?></th>
		<th><?php echo __('Meter Type Id'); ?></th>
		<th><?php echo __('Rate Type'); ?></th>
		<th><?php echo __('Sequence No'); ?></th>
		<th><?php echo __('Reading'); ?></th>
		<th><?php echo __('Finding'); ?></th>
		<th><?php echo __('Action Taken'); ?></th>
		<th><?php echo __('Note'); ?></th>
		<th><?php echo __('Temporary Meter No'); ?></th>
		<th><?php echo __('Member Present'); ?></th>
		<th><?php echo __('Attested By'); ?></th>
		<th><?php echo __('Informer Id'); ?></th>
		<th><?php echo __('Submitted'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($informer['InspectionReport'] as $inspectionReport): ?>
		<tr>
			<td><?php echo $inspectionReport['id']; ?></td>
			<td><?php echo $inspectionReport['created']; ?></td>
			<td><?php echo $inspectionReport['modified']; ?></td>
			<td><?php echo $inspectionReport['user_id']; ?></td>
			<td><?php echo $inspectionReport['inspection_date']; ?></td>
			<td><?php echo $inspectionReport['tip_off_id']; ?></td>
			<td><?php echo $inspectionReport['electric_cooperatives_id']; ?></td>
			<td><?php echo $inspectionReport['consumer_name']; ?></td>
			<td><?php echo $inspectionReport['representative']; ?></td>
			<td><?php echo $inspectionReport['current_address']; ?></td>
			<td><?php echo $inspectionReport['billing_address']; ?></td>
			<td><?php echo $inspectionReport['meter_no']; ?></td>
			<td><?php echo $inspectionReport['meter_type_id']; ?></td>
			<td><?php echo $inspectionReport['rate_type']; ?></td>
			<td><?php echo $inspectionReport['sequence_no']; ?></td>
			<td><?php echo $inspectionReport['reading']; ?></td>
			<td><?php echo $inspectionReport['finding']; ?></td>
			<td><?php echo $inspectionReport['action_taken']; ?></td>
			<td><?php echo $inspectionReport['note']; ?></td>
			<td><?php echo $inspectionReport['temporary_meter_no']; ?></td>
			<td><?php echo $inspectionReport['member_present']; ?></td>
			<td><?php echo $inspectionReport['attested_by']; ?></td>
			<td><?php echo $inspectionReport['informer_id']; ?></td>
			<td><?php echo $inspectionReport['submitted']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'inspection_reports', 'action' => 'view', $inspectionReport['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'inspection_reports', 'action' => 'edit', $inspectionReport['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'inspection_reports', 'action' => 'delete', $inspectionReport['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $inspectionReport['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Inspection Report'), array('controller' => 'inspection_reports', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
				
			</div><!-- /.related -->

					
			<div class="related">

				<h3><?php echo __('Related Rewards'); ?></h3>
				
				<?php if (!empty($informer['Reward'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
											<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Tip Off Id'); ?></th>
		<th><?php echo __('Electric Cooperatives Id'); ?></th>
		<th><?php echo __('Payment Schedule Id'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Ime Amount'); ?></th>
		<th><?php echo __('Informer Id'); ?></th>
		<th><?php echo __('Smartmoney Card No'); ?></th>
		<th><?php echo __('Reward Status Id'); ?></th>
		<th><?php echo __('Reference No'); ?></th>
		<th><?php echo __('Notify Informer'); ?></th>
		<th><?php echo __('Bill Date'); ?></th>
		<th><?php echo __('Date Rewarded'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($informer['Reward'] as $reward): ?>
		<tr>
			<td><?php echo $reward['id']; ?></td>
			<td><?php echo $reward['created']; ?></td>
			<td><?php echo $reward['modified']; ?></td>
			<td><?php echo $reward['user_id']; ?></td>
			<td><?php echo $reward['tip_off_id']; ?></td>
			<td><?php echo $reward['electric_cooperatives_id']; ?></td>
			<td><?php echo $reward['payment_schedule_id']; ?></td>
			<td><?php echo $reward['amount']; ?></td>
			<td><?php echo $reward['ime_amount']; ?></td>
			<td><?php echo $reward['informer_id']; ?></td>
			<td><?php echo $reward['smartmoney_card_no']; ?></td>
			<td><?php echo $reward['reward_status_id']; ?></td>
			<td><?php echo $reward['reference_no']; ?></td>
			<td><?php echo $reward['notify_informer']; ?></td>
			<td><?php echo $reward['bill_date']; ?></td>
			<td><?php echo $reward['date_rewarded']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'rewards', 'action' => 'view', $reward['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'rewards', 'action' => 'edit', $reward['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'rewards', 'action' => 'delete', $reward['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $reward['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Reward'), array('controller' => 'rewards', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
				
			</div><!-- /.related -->

					
			<div class="related">

				<h3><?php echo __('Related Tip Offs'); ?></h3>
				
				<?php if (!empty($informer['TipOff'])): ?>
					
					<div class="table-responsive">
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
											<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Code'); ?></th>
		<th><?php echo __('Informer Id'); ?></th>
		<th><?php echo __('Electric Cooperatives Id'); ?></th>
		<th><?php echo __('Description'); ?></th>
		<th><?php echo __('Address'); ?></th>
		<th><?php echo __('Tip Off Status Id'); ?></th>
		<th><?php echo __('Finding Id'); ?></th>
		<th><?php echo __('Area Id'); ?></th>
		<th><?php echo __('Date Assigned'); ?></th>
		<th><?php echo __('Date Confirmed'); ?></th>
		<th><?php echo __('Date For Settlement'); ?></th>
		<th><?php echo __('Date Settled'); ?></th>
		<th><?php echo __('Tip Off Type Id'); ?></th>
									<th class="actions"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($informer['TipOff'] as $tipOff): ?>
		<tr>
			<td><?php echo $tipOff['id']; ?></td>
			<td><?php echo $tipOff['created']; ?></td>
			<td><?php echo $tipOff['modified']; ?></td>
			<td><?php echo $tipOff['code']; ?></td>
			<td><?php echo $tipOff['informer_id']; ?></td>
			<td><?php echo $tipOff['electric_cooperatives_id']; ?></td>
			<td><?php echo $tipOff['description']; ?></td>
			<td><?php echo $tipOff['address']; ?></td>
			<td><?php echo $tipOff['tip_off_status_id']; ?></td>
			<td><?php echo $tipOff['finding_id']; ?></td>
			<td><?php echo $tipOff['area_id']; ?></td>
			<td><?php echo $tipOff['date_assigned']; ?></td>
			<td><?php echo $tipOff['date_confirmed']; ?></td>
			<td><?php echo $tipOff['date_for_settlement']; ?></td>
			<td><?php echo $tipOff['date_settled']; ?></td>
			<td><?php echo $tipOff['tip_off_type_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'tip_offs', 'action' => 'view', $tipOff['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'tip_offs', 'action' => 'edit', $tipOff['id']), array('class' => 'btn btn-default btn-xs')); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'tip_offs', 'action' => 'delete', $tipOff['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $tipOff['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				<div class="actions">
					<?php echo $this->Html->link('<i class="icon-plus icon-white"></i> '.__('New Tip Off'), array('controller' => 'tip_offs', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>				</div><!-- /.actions -->
				
			</div><!-- /.related -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
