

				<?php 
					foreach ($feedbacks as $feedback): 
					if($feedback['Feedback']['type'] == "[ + ]"){ $type = "glyphicon glyphicon-plus"; $typeAlt = "Positive feedback"; } else { $type = "glyphicon glyphicon-minus"; $typeAlt = "Negative feedback"; }
				?>
					<blockquote>
						<cite><?php echo h($feedback['Feedback']['created']); ?>&nbsp;</cite><cite class="pull-right"><?php echo h($feedback['ElectricCooperatives']['abbreviation']); ?>&nbsp;</cite>
					  	<p><span class="<?php echo $type; ?>" title="<?php echo $typeAlt; ?>"></span><?php echo h(" ".$feedback['Feedback']['message']); ?>&nbsp;</p>
					  	<small class="pull-right"> <cite title="Source Title"><?php echo $feedback['Informer']['code']; ?></cite></small>
					</blockquote>

				<?php endforeach; ?>
				<?php if(!$feedbacks){ echo "<p class='text-center'><b>No feedbacks...</b></p>"; } ?>
			<hr>
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} feedbacks out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->
			
