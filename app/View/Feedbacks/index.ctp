<script type="text/javascript">
	$('document').ready(function(){
		$('#electric_cooperatives_id').change(function(){
			var ecId = $(this).val();
			$('.feedbacks').load('<?php echo $this->base; ?>/feedbacks/feedbacks/'+ecId);
		});

	});
</script>


<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		<br><br>
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('New Feedback'), array('action' => 'add'), array('class' => '')); ?></li>
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<h2><?php echo __('Feedbacks'); ?></h2>
			
			<div class="form-group">
				<?php echo $this->Form->label('electric_cooperatives_id', 'Electric Company');?>
				<?php echo $this->Form->input('electric_cooperatives_id', array('empty' => 'Please select', 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		<div class="feedbacks index">

				<?php 
					foreach ($feedbacks as $feedback): 
					if($feedback['Feedback']['type'] == "[ + ]"){ $type = "glyphicon glyphicon-plus"; $typeAlt = "Positive feedback"; } else { $type = "glyphicon glyphicon-minus"; $typeAlt = "Negative feedback"; }
				?>
					<blockquote>
						<cite><?php echo h($feedback['Feedback']['created']); ?>&nbsp;</cite><cite class="pull-right"><?php echo h($feedback['ElectricCooperatives']['abbreviation']); ?>&nbsp;</cite>
					  	<p><span class="<?php echo $type; ?>" title="<?php echo $typeAlt; ?>"></span><?php echo h(" ".$feedback['Feedback']['message']); ?>&nbsp;</p>
					  	<small class="pull-right"> <cite title="Source Title"><?php echo $feedback['Informer']['code']; ?></cite></small>
					</blockquote>

				<?php endforeach; ?>
			
			<hr>
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} feedbacks out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
