<div id="page-container" class="row"> 
	
	<div id="sidebar" class="col-sm-3"></div>			
				
	<div id="page-content" class="col-sm-6">
		<legend><?php echo __('Contact Us'); ?></legend>

		<div class="contact_us form">
		
			<?php echo $this->Form->create('Page', array('action' => 'contact_us', 'inputDefaults' => array('label' => false), 'role' => 'form')); ?>
			<fieldset>
			<div class="form-group">
				<?php echo $this->Form->label('subject', 'Subject');?>
					<?php echo $this->Form->input('subject', array('class' => 'form-control', 'required' => 'required')); ?>
			</div><!-- .form-group -->
			<div class="form-group">
				<?php echo $this->Form->label('message', 'Message');?>
					<?php echo $this->Form->input('message', array('type' => 'textarea', 'class' => 'form-control', 'required' => 'required')); ?>
			</div><!-- .form-group -->
			<?php if(!AuthComponent::User()){ ?>
			<br /><br />
			<div class="form-group">
				<legend><?php echo __('Reporter\'s Contacts'); ?></legend>
			</div>	
			<div class="alert well alert">
				<p>All fields below are optional. Mobile number and email address will be used for real-time push confirmation and updates only. Your personal data will not be shared and will not be displayed to anyone.</p>
			</div>	

					<div class="form-group">
						<?php echo $this->Form->label('email', 'Email');?>
							<?php echo $this->Form->input('email', array('class' => 'form-control', 'type' => 'email', 'placeholder' => 'email@domain.com')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('mobile', 'Mobile');?>
							<?php echo $this->Form->input('mobile', array('class' => 'form-control', 'placeholder' => '63923XXXXXXX')); ?>
					</div><!-- .form-group -->
			<?php } ?>

			</fieldset>

			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->
	
	<div id="sidebar" class="col-sm-3"></div>

</div><!-- /#page-container .row-fluid -->
<?php echo $this->Html->script(array('libs/jquery.validate.min', 'customs/validations.js')); ?>