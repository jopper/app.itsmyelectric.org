
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
		
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="users form">
		
			<?php echo $this->Form->create('User', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Change Password'); ?></h2>
					<div class="form-group">
							<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('current_password', 'Current Password');?>
							<?php echo $this->Form->input('current_password', array('class' => 'form-control', 'type' => 'password')); ?>
							<?php echo $this->Form->input('password', array('class' => 'form-control', 'type' => 'hidden')); ?>
					</div><!-- .form-group -->


					<div class="form-group">
						<?php echo $this->Form->label('new_password', 'New Password');?>
							<?php echo $this->Form->input('new_password', array('class' => 'form-control', 'type' => 'password')); ?>
					</div><!-- .form-group -->

					<div class="form-group">
						<?php echo $this->Form->label('confirm_password', 'Confirm Password');?>
							<?php echo $this->Form->input('confirm_password', array('class' => 'form-control', 'type' => 'password')); ?>
					</div><!-- .form-group -->

				</fieldset>
					<div class="col-xs-6 col-sm-6 col-lg-4">
						<?php echo $this->Html->link(__('Cancel'), array('action' => 'view', $this->request->data['User']['id']), array('class' => 'btn btn-large btn-primary pull-left')); ?>
						<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary pull-right')); ?>
					</div>
					<?php echo $this->Form->end(); ?>
							
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
