
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">			
				<li class="list-group-item"><?php echo $this->Html->link(__('Update Contacts'), array('action' => 'update_contacts', $user['User']['id']), array('class' => '')); ?> </li>
				
				<li class="list-group-item"><?php echo $this->Html->link(__('Change Password'), array('action' => 'change_password', $user['User']['id']), array('class' => '')); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="users view">

			<h2><?php echo h($user['User']['code']); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>		
							<td><strong><?php echo __('Created'); ?></strong></td>
							<td>
								<?php echo h($user['User']['created']); ?>
								&nbsp;
							</td>
						</tr>
						<!-- <tr>		
							<td><strong><?php echo __('Code'); ?></strong></td>
							<td>
								<?php echo h($user['User']['code']); ?>
								&nbsp;
							</td>
						</tr> -->
						<!-- <tr>		
							<td><strong><?php echo __('Type'); ?></strong></td>
							<td>
								<?php echo h($user['User']['type']); ?>
								&nbsp;
							</td>
						</tr> -->
						<tr>		
							<td><strong><?php echo __('Mobile'); ?></strong></td>
							<td>
								<?php echo h($user['User']['mobile']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Email'); ?></strong></td>
							<td>
								<?php echo h($user['User']['email']); ?>
								&nbsp;
							</td>
						</tr>
						<tr>		
							<td><strong><?php echo __('Password'); ?></strong></td>
							<td>
								Enrypted
								&nbsp;
							</td>
						</tr>			
					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
