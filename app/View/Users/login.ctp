


<?php echo $this->Form->create('User', array('class' => 'form-signin')); ?>
	<h2 class="form-signin-heading">Sign in</h2>
<?php	
	
	echo $this->Form->input('username', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'codename'));
	echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'div' => false, 'placeholder' => 'password'));
	echo $this->Form->button('Login', array('class' => 'btn btn-lg btn-primary btn-block', 'type' => 'submit'));
	echo $this->Form->end();

?>


