
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">			
						<li class="list-group-item"><?php echo $this->Html->link(__('Edit Report Evidence'), array('action' => 'edit', $tipOffEvidence['TipOffEvidence']['id']), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Report Evidence'), array('action' => 'delete', $tipOffEvidence['TipOffEvidence']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $tipOffEvidence['TipOffEvidence']['id'])); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Report Evidences'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('My Reports'), array('controller' => 'tip_offs', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Report'), array('controller' => 'tip_offs', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="tipOffEvidences view">

			<h2><?php  echo __('Report Evidence'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($tipOffEvidence['TipOffEvidence']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($tipOffEvidence['TipOffEvidence']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($tipOffEvidence['TipOffEvidence']['modified']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Report Code'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($tipOffEvidence['TipOff']['code'], array('controller' => 'tip_offs', 'action' => 'view', $tipOffEvidence['TipOff']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('File Name'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($tipOffEvidence['TipOffEvidence']['file_name'], array('action' => 'view_file', $tipOffEvidence['TipOffEvidence']['id'], $tipOffEvidence['TipOff']['id']), array('class' => '', 'target' => '_BLANK')); ?>
			&nbsp;
		</td>
</tr>
<tr>		<td><strong><?php echo __('Dimension'); ?></strong></td>
		<td>
			<?php echo h($tipOffEvidence['TipOffEvidence']['dimension'].' pixels'); ?>
			&nbsp;
		</td>
</tr>
<tr>		<td><strong><?php echo __('Size'); ?></strong></td>
		<td>
			<?php echo h(number_format($tipOffEvidence['TipOffEvidence']['size'])).' bytes'; ?>
			&nbsp;
		</td>
</tr>
<tr>		<td><strong><?php echo __('Details'); ?></strong></td>
		<td>
			<?php echo h($tipOffEvidence['TipOffEvidence']['details']); ?>
			&nbsp;
		</td>
</tr>				</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
