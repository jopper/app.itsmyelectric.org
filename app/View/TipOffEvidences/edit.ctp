
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
										<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('TipOffEvidence.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('TipOffEvidence.id'))); ?></li>
										<li class="list-group-item"><?php echo $this->Html->link(__('List Tip Off Evidences'), array('action' => 'index')); ?></li>
						<li class="list-group-item"><?php echo $this->Html->link(__('List Tip Offs'), array('controller' => 'tip_offs', 'action' => 'index')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Tip Off'), array('controller' => 'tip_offs', 'action' => 'add')); ?> </li>
			</ul><!-- /.list-group -->
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="tipOffEvidences form">
		
			<?php echo $this->Form->create('TipOffEvidence', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
			<fieldset>
					<h2><?php echo __('Edit Tip Off Evidence'); ?></h2>
			<div class="form-group">
				<?php echo $this->Form->input('id', array('class' => 'form-control')); ?>
			</div><!-- .form-group -->
			<div class="form-group">
				<label class="control-label">Report Code</label>
				<br>
				<label class="control-label"><?php echo $this->Form->value('TipOff.code'); ?></label>
			</div><!-- .form-group -->
			<div class="form-group">
				<?php echo $this->Form->label('details', 'Details');?>
				<?php echo $this->Form->input('details', array('class' => 'form-control')); ?>
			</div><!-- .form-group -->

			</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
			<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
