
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">						
				<li class="list-group-item"><?php echo $this->Html->link(__('List Reports'), array('controller' => 'tip_offs', 'action' => 'index'), array('class' => '')); ?></li> 
				<li class="list-group-item"><?php echo $this->Html->link(__('New Report'), array('controller' => 'tip_offs', 'action' => 'add'), array('class' => '')); ?></li> 
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="tipOffEvidences index">
		
			<h2><?php echo __('Report Evidences'); ?></h2>
			
			<div class="table-responsive">
				<table cellpadding="0" cellspacing="0" class="table table-striped table-bordered">
					<thead>
						<tr>
															<th><?php echo 'Thumbnail'; ?></th>
															<th><?php echo $this->Paginator->sort('created'); ?></th>
															<th><?php echo $this->Paginator->sort('modified'); ?></th>
															<th><?php echo $this->Paginator->sort('tip_off_id'); ?></th>
															<th><?php echo $this->Paginator->sort('file_name'); ?></th>
															<th><?php echo $this->Paginator->sort('details'); ?></th>
															<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($tipOffEvidences as $tipOffEvidence): ?>
	<tr>
		<td><?php if($tipOffEvidence['TipOffEvidence']['new_file_name']){ echo $this->Image->resize($tipOffEvidence['TipOffEvidence']['new_file_name'], 100, 100); } ?>&nbsp;</td>
		<td><?php echo h($tipOffEvidence['TipOffEvidence']['created']); ?>&nbsp;</td>
		<td><?php echo h($tipOffEvidence['TipOffEvidence']['modified']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($tipOffEvidence['TipOff']['code'], array('controller' => 'tip_offs', 'action' => 'view', $tipOffEvidence['TipOff']['id'])); ?>
		</td>
		<td><?php echo h($tipOffEvidence['TipOffEvidence']['file_name']); ?>&nbsp;</td>
		<td><?php echo h($tipOffEvidence['TipOffEvidence']['details']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $tipOffEvidence['TipOffEvidence']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $tipOffEvidence['TipOffEvidence']['id']), array('class' => 'btn btn-default btn-xs')); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $tipOffEvidence['TipOffEvidence']['id']), array('class' => 'btn btn-default btn-xs'), __('Are you sure you want to delete # %s?', $tipOffEvidence['TipOffEvidence']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
