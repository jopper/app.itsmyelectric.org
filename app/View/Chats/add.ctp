
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('Messages'), array('action' => 'index')); ?></li>
			</ul><!-- /.list-group -->
		
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

		<div class="chats form">
		
			<?php echo $this->Form->create('Chat', array('inputDefaults' => array('label' => false), 'role' => 'form')); ?>
				<fieldset>
					<h2><?php echo __('Add Message'); ?></h2>
			<div class="form-group">

			<div class="form-group">
				<?php echo $this->Form->label('electric_cooperatives_id', 'Electric Company');?>
					<?php echo $this->Form->input('electric_cooperatives_id', array('class' => 'form-control', 'empty' => 'Please select', 'required')); ?>
			</div><!-- .form-group -->

			<div class="form-group">
				<?php echo $this->Form->label('message', 'Message');?>
					<?php echo $this->Form->input('message', array('type' => 'textarea', 'class' => 'form-control', 'required')); ?>
			</div><!-- .form-group -->





				</fieldset>
			<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>
<?php echo $this->Form->end(); ?>
			
		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
