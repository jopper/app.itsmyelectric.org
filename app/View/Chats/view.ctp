
<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		
		<div class="actions">
			
			<ul class="list-group">			
						<li class="list-group-item"><?php echo $this->Html->link(__('Edit Chat'), array('action' => 'edit', $chat['Chat']['id']), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Form->postLink(__('Delete Chat'), array('action' => 'delete', $chat['Chat']['id']), array('class' => ''), __('Are you sure you want to delete # %s?', $chat['Chat']['id'])); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Chats'), array('action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Chat'), array('action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Informers'), array('controller' => 'informers', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Informer'), array('controller' => 'informers', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Electric Cooperatives'), array('controller' => 'electric_cooperatives', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Electric Cooperatives'), array('controller' => 'electric_cooperatives', 'action' => 'add'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('List Delivery Statuses'), array('controller' => 'delivery_statuses', 'action' => 'index'), array('class' => '')); ?> </li>
		<li class="list-group-item"><?php echo $this->Html->link(__('New Delivery Status'), array('controller' => 'delivery_statuses', 'action' => 'add'), array('class' => '')); ?> </li>
				
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .span3 -->
	
	<div id="page-content" class="col-sm-9">
		
		<div class="chats view">

			<h2><?php  echo __('Chat'); ?></h2>
			
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Date Created'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['date_created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Date Sent'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['date_sent']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Code'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['code']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Mobile No'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['mobile_no']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('User'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($chat['User']['id'], array('controller' => 'users', 'action' => 'view', $chat['User']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Informer'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($chat['Informer']['code'], array('controller' => 'informers', 'action' => 'view', $chat['Informer']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Message'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['message']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Electric Cooperatives'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($chat['ElectricCooperatives']['id'], array('controller' => 'electric_cooperatives', 'action' => 'view', $chat['ElectricCooperatives']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Type'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['type']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Processed'); ?></strong></td>
		<td>
			<?php echo h($chat['Chat']['processed']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Delivery Status'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($chat['DeliveryStatus']['id'], array('controller' => 'delivery_statuses', 'action' => 'view', $chat['DeliveryStatus']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
