<script type="text/javascript">
	$('document').ready(function(){
		$('#electric_cooperatives_id').change(function(){
			var ecId = $(this).val();
			$('.chats').load('<?php echo $this->base; ?>/chats/chats/'+ecId);
		});

	});
</script>


<div id="page-container" class="row">

	<div id="sidebar" class="col-sm-3">
		<br><br>
		<div class="actions">
		
			<ul class="list-group">
				<li class="list-group-item"><?php echo $this->Html->link(__('New Message'), array('action' => 'add'), array('class' => '')); ?></li>						
			</ul><!-- /.list-group -->
			
		</div><!-- /.actions -->
		
	</div><!-- /#sidebar .col-sm-3 -->
	
	<div id="page-content" class="col-sm-9">

			<h2><?php echo __('Hotline'); ?></h2>

			<div class="form-group">
				<?php echo $this->Form->label('electric_cooperatives_id', 'Electric Company');?>
				<?php echo $this->Form->input('electric_cooperatives_id', array('empty' => 'Please select', 'label'=>false, 'class' => 'form-control')); ?>
			</div>
		<div class="chats index">
		
			
			<?php
				foreach ($chats as $chat): 
					if($chat['Chat']['type'] == "out"){ $chatter = $chat['ElectricCooperatives']['abbreviation']; $class = "bubble-left"; } else { $chatter = "Me"; $class = "bubble-right text-right"; }
			?>
			<div class="<?php echo $class; ?>">
			  <strong><?php echo h($chatter); ?>,&nbsp;</strong><?php echo h($chat['Chat']['date_created']); ?>  
			  <p><?php echo h($chat['Chat']['message']); ?>&nbsp;</p>
			</div>
			
			<?php endforeach; ?>
			<div class="col-sm-9">
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>
			</div>	

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul> <!-- /.pagination -->
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->
