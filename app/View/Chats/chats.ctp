<?php
				foreach ($chats as $chat): 
					if($chat['Chat']['type'] == "out"){ $chatter = $chat['ElectricCooperatives']['abbreviation']; $class = "bubble-left"; } else { $chatter = "Me"; $class = "bubble-right text-right"; }
			?>
			<div class="<?php echo $class; ?>">
			  <strong><?php echo h($chatter); ?>,&nbsp;</strong><?php echo h($chat['Chat']['date_created']); ?>  
			  <p><?php echo h($chat['Chat']['message']); ?>&nbsp;</p>
			</div>
			
			<?php endforeach; ?>
			<?php if(!$chats){ echo "<p class='text-center'><b>No messages...</b></p>"; } ?>
			<div class="col-sm-9">
			<p><small>
				<?php
				echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
				));
				?>			</small></p>
			</div>	

			<ul class="pagination">
				<?php
		echo $this->Paginator->prev('< ' . __('Previous'), array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
		echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'tag' => 'li', 'currentClass' => 'disabled'));
		echo $this->Paginator->next(__('Next') . ' >', array('tag' => 'li'), null, array('class' => 'disabled', 'tag' => 'li', 'disabledTag' => 'a'));
	?>
			</ul><!-- /.pagination -->